/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A simple class for making connections to SQLite databases
 *
 * @author Gage
 */
public class JDBCfactory {

    String filename;

    public JDBCfactory() {
        try {
            this.filename = Settings.getAppDirectory() + "db.sqlite";
        } catch (Exception ex) {
            this.filename = "db.sqlite";
            System.err.println("failed to create database in propper place!"
                    + "\n ");
            ex.printStackTrace();
        }
    }

    public JDBCfactory(boolean testing) {
        if (testing) {
            this.filename = "testingDB.sqlite";
        } else {
            this.filename = "db.sqlite";
        }
    }

    public JDBCfactory(String filename) {
        this.filename = filename;
    }

    public Connection createConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:sqlite:" + filename);
    }

    /**
     * This will create a fake in memory db for testing. So the database will be
     * gone after the program closes.
     *
     * @return
     * @throws SQLException
     */
    public Connection createFakeConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:sqlite::memory:");
    }

}
