package foodNanny.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is a database abstraction of the Buy List.
 *
 * @author justgage
 */
public class BuyListdb extends Listdb {

    /**
     * A constructor all DB objects should have
     *
     * @param jdbcfac
     */
    public BuyListdb(JDBCfactory jdbcfac) throws SQLException {
        this.tableName = "buyList";
        this.jdbcFactory = jdbcfac;
        this.createTable();

    }
    
    public void add(String barcode, int qty) throws SQLException {
        ProductTypesdb pt = new ProductTypesdb(jdbcFactory);

        ProductDBO product = pt.getByBarcode(barcode);

        for (int i = 0; i < qty; i++) {
            add(product.getID());
        }
    }
}
