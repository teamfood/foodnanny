/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.time.LocalDateTime;

/**
 *
 * @author Gage
 */
public class EventDBO {

    private int ID;
    private int type;
    private String message;
    private LocalDateTime date;

    public EventDBO(int ID, int type, String message, LocalDateTime date) {
        this.ID = ID;
        this.type = type;
        this.message = message;
        this.date = date;
    }

    public EventDBO() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

}
