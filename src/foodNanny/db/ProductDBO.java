/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.util.Date;

/**
 *
 * @author Gage
 */
public class ProductDBO {

    private int ID;
    private int productTypeID;
    private String name;
    private int daysGood;
    private String barcode;
    private int wholesellerID;
    private int maxQty;
    private int qty;
    private double price;
    private Date dateAdded;
    private int qtyRemoved;

    public int getQtyRemoved() {
        return qtyRemoved;
    }

    public void setQtyRemoved(int qtyRemoved) {
        this.qtyRemoved = qtyRemoved;
    }

    public ProductDBO() {

    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getProductTypeID() {
        return productTypeID;
    }

    public void setProductTypeID(int productTypeID) {
        this.productTypeID = productTypeID;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDaysGood() {
        return daysGood;
    }

    public void setDaysGood(int daysGood) {
        this.daysGood = daysGood;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getWholesellerID() {
        return wholesellerID;
    }

    public void setWholesellerID(int wholesellerID) {
        this.wholesellerID = wholesellerID;
    }

    public int getMaxQty() {
        return maxQty;
    }

    public void setMaxQty(int maxQty) {
        this.maxQty = maxQty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return name + "(" + ID + ") " + " : \n"
                + "\tprice: " + price + "\n"
                + "\tmaxQty: " + maxQty + "\n"
                + "\twholesellerID : " + wholesellerID + "\n"
                + "\tbarcode : " + barcode + "\n"
                + "\tdaysGood : " + daysGood + "\n"
                + "\tdateAdded : " + dateAdded + "\n"
                + "\n";
    }

}
