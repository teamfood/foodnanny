/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gage
 */
public class Wholesellerdb {

    /**
     * A constructor all DB objects should have
     *
     * @param db
     */
    private final JDBCfactory factory;
    private final String tableName;

    public Wholesellerdb(JDBCfactory factory) throws SQLException {
        this.tableName = "wholeseller";
        this.factory = factory;

        createTable();
    }

    /**
     * This will create a table for this object if it doesn't exist.
     *
     * @throws SQLException
     */
    private void createTable() throws SQLException {

        Connection con = factory.createConnection();
        Statement statement = con.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " ("
                + "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "name TEXT,"
                + "email TEXT,"
                + "message TEXT"
                + ");";

        statement.execute(sql);
        statement.close();
        con.close();
    }

    /**
     * This will create a table for this object if it doesn't exist.
     *
     * @throws SQLException
     */
    public void deleteTable() throws SQLException {
        Connection con = null;
        Statement statement = null;
        try {
            con = factory.createConnection();
            statement = con.createStatement();
            String sql = "DROP TABLE " + tableName;

            statement.execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Wholesellerdb.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            if (statement != null) {
                statement.close();
            }
            if (con != null) {
                con.close();
            }
        }
    }

    public int add(WholesellerDBO newWhole) throws SQLException, AlreadyExistsException {
        String sql = "INSERT INTO " + tableName + ""
                + "(name, email, message)"
                + "VALUES (?, ?, ?);";

        PreparedStatement ps = null;
        Connection con = null;
        int newID = -1;
        try {

            if (exists(newWhole.getName())) {
                throw new AlreadyExistsException();
            }

            con = factory.createConnection();
            ps = con.prepareStatement(sql);

            ps.setString(1, newWhole.getName());
            ps.setString(2, newWhole.getEmail());
            ps.setString(3, newWhole.getMessage());

            boolean resultSet = ps.execute();

            if (resultSet) {
                ResultSet results = ps.getResultSet();
                newID = results.getInt("ID");
            }

        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return newID;
    }

    /**
     *
     * @return @throws SQLException
     */
    public List<WholesellerDBO> getAllShallow() throws SQLException {
        String sqlWholeseller = "SELECT * FROM " + tableName;
        Statement wholeStatement = null;
        Connection wholeCon = null;
        List<WholesellerDBO> wholeList;
        try {

            wholeCon = factory.createConnection();
            wholeStatement = wholeCon.createStatement();

            ResultSet wholeSet = wholeStatement.executeQuery(sqlWholeseller);

            wholeList = new ArrayList<>();

            while (wholeSet.next()) {
                WholesellerDBO wholeseller = new WholesellerDBO();
                wholeseller.setId(wholeSet.getInt("ID"));
                wholeseller.setName(wholeSet.getString("name"));
                wholeseller.setEmail(wholeSet.getString("email"));
                wholeseller.setMessage(wholeSet.getString("message"));
                wholeList.add(wholeseller);
            }

            return wholeList;

        } catch (SQLException ex) {
            throw ex;
        } finally {

            if (wholeStatement != null) {
                wholeStatement.close();
            }

            if (wholeCon != null) {
                wholeCon.close();
            }

        }
    }

    /**
     * Update a wholesellers information
     *
     * @param whole
     * @throws java.sql.SQLException
     */
    public void update(WholesellerDBO whole) throws SQLException {
        String sql = "UPDATE " + tableName + "\n"
                + "SET name=?, email=?, message=?\n"
                + "WHERE id=?";

        PreparedStatement ps = null;
        Connection con = null;

        try {
            con = factory.createConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, whole.getName());
            ps.setString(2, whole.getEmail());
            ps.setString(3, whole.getMessage());
            ps.setInt(4, whole.getId());
            ps.executeUpdate();

        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

    }

    public boolean exists(String name) throws SQLException {
        String sql = "SELECT COUNT(*) from " + tableName + "\n"
                + "WHERE name = ?";
        Connection con = null;
        PreparedStatement statement = null;
        ResultSet result;
        int count;

        try {
            con = factory.createConnection();
            statement = con.prepareStatement(sql);
            statement.setString(1, name);
            result = statement.executeQuery();

            if (!result.isBeforeFirst()) {
                throw new SQLException("Wholesellerdb counting failed");
            }

            count = result.getInt("COUNT(*)");

            return (count > 0);

        } catch (SQLException ex) {
            throw ex;
        } finally {

            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }
        }

    }

}
