/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author justgage
 */
public class ProductTypesdb {

    private final String tableName;
    private final JDBCfactory jdbcFactory;

    public ProductTypesdb(JDBCfactory jdbcfac) throws SQLException {
        this.tableName = "productType";
        this.jdbcFactory = jdbcfac;
        createTable();
    }
    
      public void deleteTable() throws SQLException {
        Connection con = null;
        Statement statement = null;
        try {
            con = jdbcFactory.createConnection();
            statement = con.createStatement();
            String sql = "DROP TABLE " + tableName;

            statement.execute(sql);
        } catch (SQLException ex) {
            throw ex;
        } finally {

            if (statement != null) {
                statement.close();
            }
            if (con != null) {
                con.close();
            }
        }
    }
    

    /**
     * This will create a table for this object if it doesn't exist.
     *
     * @throws SQLException
     */
    private void createTable() throws SQLException {

        Connection con = null;
        con = jdbcFactory.createConnection();
        Statement statement = con.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " ("
                + "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "name TEXT,"
                + "daysGood INTEGER,"
                + "barcode TEXT,"
                + "wholesellerID INTEGER,"
                + "maxQty INTEGER,"
                + "Price REAL"
                + ");";

        statement.execute(sql);
        statement.close();
        con.close();
    }

    /**
     * Add a new product type to the database
     *
     * @param name
     * @param daysGood
     * @param barcode
     * @param wholesellerID
     * @param maxQty
     * @param price
     * @return the id of the newly added entry
     * @throws java.sql.SQLException
     * @throws foodNanny.db.AlreadyExistsException
     */
    public int add(String name, int daysGood, String barcode, int wholesellerID,
            int maxQty, double price) throws SQLException, AlreadyExistsException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            
            if(exists(name, barcode)) {
                throw new AlreadyExistsException();
            };

            String sql = "INSERT INTO " + tableName + "\n"
                    + "("
                    + "name,"
                    + "daysGood,"
                    + "barcode,"
                    + "wholesellerID,"
                    + "maxQty,"
                    + "Price"
                    + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?);";

            con = jdbcFactory.createConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, name);
            ps.setInt(2, daysGood);
            ps.setString(3, barcode);
            ps.setInt(4, wholesellerID);
            ps.setInt(5, maxQty);
            ps.setDouble(6, price);
            boolean resultSet = ps.execute();
            if (resultSet) {
                ResultSet results = ps.getResultSet();
                return results.getInt("ID");
            }

            return -1;
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (con != null) {
                con.close();
            }
        }
    }

    /**
     * Will lookup the product type by the barcode
     *
     * @param barcode
     * @return
     * @throws java.sql.SQLException
     */
    public ProductDBO getByBarcode(String barcode) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {

            String sql = "SELECT * FROM " + tableName + "\n"
                    + "WHERE barcode=?;";

            con = jdbcFactory.createConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, barcode);
            ps.execute();
            ResultSet set = ps.getResultSet();

            // if the result set is empty return null
            if (set == null || !set.isBeforeFirst()) {
                return null;
            }

            set.next();
            return resultsToDBO(set);

        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (con != null) {
                con.close();
            }
        }
    }

    /**
     * Returns a product type by it's ID
     *
     * @param ID
     * @return
     * @throws SQLException
     */
    public ProductDBO getByID(int ID) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {

            String sql = "SELECT * FROM " + tableName + "\n"
                    + "WHERE ID=?;";

            con = jdbcFactory.createConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, ID);
            ps.execute();
            ResultSet set = ps.getResultSet();

            // if the result set is empty return null
            if (set == null || !set.isBeforeFirst()) {
                return null;
            }

            set.next();
            return resultsToDBO(set);

        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (con != null) {
                con.close();
            }
        }
    }

    private ProductDBO resultsToDBO(ResultSet set) throws SQLException {

        ProductDBO product = new ProductDBO();

        product.setID(set.getInt("ID"));
        product.setName(set.getString("name"));
        product.setDaysGood(set.getInt("daysGood"));
        product.setBarcode(set.getString("barcode"));
        product.setWholesellerID(set.getInt("wholesellerID"));
        product.setMaxQty(set.getInt("maxQty"));
        product.setPrice(set.getDouble("Price"));
        return product;

    }

    /**
     * @return A map of ProductDBO's with their ID as their key
     * @throws SQLException
     */
    public Map<Integer, ProductDBO> getAll() throws SQLException {
        Connection con = null;
        Statement statement = null;
        Map<Integer, ProductDBO> map = new HashMap<>();
        try {

            String sql = "SELECT * FROM " + tableName;

            con = jdbcFactory.createConnection();
            statement = con.createStatement();

            ResultSet set = statement.executeQuery(sql);

            while (set.next()) {
                map.put(set.getInt("ID"), resultsToDBO(set));
            }

            return map;

        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }
        }

    }

    /**
     * @return A map of ProductDBO's with their ID as their key
     * @throws SQLException
     */
    public Map<Integer, Map<Integer, ProductDBO>> getAllbyWholseller() throws SQLException {
        Connection con = null;
        Statement statement = null;

        Map<Integer, Map<Integer, ProductDBO>> map = new HashMap<>();

        try {

            String sql = "SELECT * FROM " + tableName;

            con = jdbcFactory.createConnection();
            statement = con.createStatement();

            ResultSet set = statement.executeQuery(sql);

            if (!set.isBeforeFirst()) {
                con.close();
                statement.close();
                return map;
            }

            set.next();
            ProductDBO tempProduct = resultsToDBO(set);

            while (set.next()) {

                if (map.containsKey(tempProduct.getWholesellerID())) {
                    map.get(tempProduct.getWholesellerID()).put(tempProduct.getID(), tempProduct);
                } else {
                    map.put(tempProduct.getWholesellerID(), new HashMap<>());
                    map.get(tempProduct.getWholesellerID()).put(tempProduct.getID(), tempProduct);
                }

            }

            return map;

        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }
        }

    }

    public boolean exists(String name, String barcode) throws SQLException {
        String sql = "SELECT COUNT(*) from " + tableName + "\n"
                + "WHERE name = ?\n"
                + "OR barcode = ?";
        Connection con = null;
        PreparedStatement statement = null;
        ResultSet result;
        int count;

        try {
            con = jdbcFactory.createConnection();
            statement = con.prepareStatement(sql);
            statement.setString(1, name);
            statement.setString(2, barcode);
            result = statement.executeQuery();

            if (!result.isBeforeFirst()) {
                throw new SQLException("ProductTypesdb counting failed");
            }

            count = result.getInt("COUNT(*)");

            return (count > 0);

        } catch (SQLException ex) {
            throw ex;
        } finally {

            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }
        }

    }
}
