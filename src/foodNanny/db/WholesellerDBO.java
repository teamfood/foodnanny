/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Gage
 */
public class WholesellerDBO {

    int id;
    String name;
    String email;
    String eMessage;
    Map<Integer, ProductDBO> products;

    public WholesellerDBO(String name, String email, String message) {
        this.name = name;
        this.email = email;
        this.eMessage = message;
    }

    public WholesellerDBO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<Integer, ProductDBO> getProducts() {
        return products;
    }

    public void setProducts(Map<Integer, ProductDBO> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return name;
    }

    public String toStringVarbose() {
        String message = new String();

        message += id + " : {\n"
                + "\t name : " + name + "\n"
                + "\t email : " + email + "\n"
                + "\t products : [\n";

        for (ProductDBO product : products.values()) {
            message += "\t\t " + product.getName()
                    + ": qty= " + product.getQty() + "\n";
        }

        message += "\n\t]\n}";

        return message;

    }

    public String getMessage() {
        return eMessage;
    }

    public void setMessage(String message) {
        this.eMessage = message;
    }

}
