/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

/**
 *
 * @author Gage
 */
public class Settings {

    private SettingsDBO theSettings;
    private static final String settingsFilename = "settings.json";

    public Settings() throws FileNotFoundException, Exception {
        theSettings = new SettingsDBO();
        load();
    }

    ;
    
    /**
     * This will load the settings from a file
     */
    private void load() throws FileNotFoundException, Exception {
        Genson genson = new Genson();
        File file = new File(getAppDirectory() + settingsFilename);
        InputStream input = new FileInputStream(file);
        theSettings = genson.deserialize(input, SettingsDBO.class);
        if (theSettings == null) {
            file.delete();
            theSettings = new SettingsDBO();
            save();
        }
    }

    /**
     * Get the application's directory WITH the trailing slash
     *
     * @return
     * @throws java.lang.Exception
     */
    public static String getAppDirectory() throws Exception {
        File folder = new File(System.getProperty("user.home")
                + File.separator + ".foodNanny" + File.separator);

        if (folder.exists() == false && folder.mkdirs() == false) {
            throw new Exception("Directorys couldn't be made");
        }

        return System.getProperty("user.home")
                + File.separator + ".foodNanny" + File.separator;
    }

    public void save() throws Exception {

        SettingsDBO settings = new SettingsDBO();
        File file = new File(getAppDirectory() + settingsFilename);
//        XStream xStream = new XStream();
        OutputStream outputStream = null;
        Writer writer = null;

        try {
            outputStream = new FileOutputStream(file);
            writer = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));

            // This will save the settings in JSON
            GensonBuilder builder = new GensonBuilder();
            // pretty print
            builder.useIndentation(true);

            Genson genson = builder.create();
            //Save the settings
            genson.serialize(theSettings, outputStream);

        } catch (FileNotFoundException exp) {

            System.err.println(exp);

            throw new Exception("Settings failed to save! -> " + exp);

        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     *
     * @return
     */
    public SettingsDBO getTheSettings() {
        return theSettings;
    }

}
