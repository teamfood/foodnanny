/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Gage
 */
public class BoughtList extends Listdb {
    BoughtList(JDBCfactory jdbcFac) throws SQLException {
        this.tableName = "stockList";
        this.jdbcFactory = jdbcFac;

        createTable();
    }
}
