/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author justgage
 */
public class StockListdb extends Listdb {

    /**
     * A constructor all DB objects should have
     *
     * @param jdbcFac
     * @throws java.sql.SQLException
     */
    public StockListdb(JDBCfactory jdbcFac) throws SQLException {
        super("stockList", jdbcFac);
    }
    /**
     * Add a number of products by barcode
     *
     * @param barcode
     * @param qty
     * @throws java.sql.SQLException
     */
    public void add(String barcode, int qty) throws SQLException {
        ProductTypesdb pt = new ProductTypesdb(jdbcFactory);

        ProductDBO product = pt.getByBarcode(barcode);

        for (int i = 0; i < qty; i++) {
            add(product.getID());
        }
    }

}
