/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gage
 */
public class Eventdb {

    JDBCfactory factory;
    String tableName = "events";

    /**
     * Constructor. Also will create a table if need be.
     *
     * @param factory
     * @throws SQLException
     */
    public Eventdb(JDBCfactory factory) throws SQLException {
        this.factory = factory;
        createTable();
    }

    /**
     * This will delete the table mostly for testing purposes
     *
     * @throws SQLException
     */
    public void deleteTable() throws SQLException {
        Statement statement = null;
        Connection con = null;

        try {
            con = factory.createConnection();
            statement = con.createStatement();
            String sql = "DROP TABLE " + tableName;

            statement.execute(sql);
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }
        }
    }

    /**
     * This will create a table for this object if it doesn't exist.
     *
     * @throws SQLException
     */
    private void createTable() throws SQLException {

        Connection con = factory.createConnection();
        Statement statement = con.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " ("
                + "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "message TEXT,"
                + "type INTEGER," // this is a code to tell if warning, or just informational message, anything you want.
                + "date TEXT"
                + ");";
        
        LocalTime time = LocalTime.now();

        statement.execute(sql);
        statement.close();
        con.close();
    }

    /**
     * Add a new notification to the database
     * @param message message to display to user
     * @param type a number that will represent a type such as Warning or such.
     * @param date at which the notification should be added for
     * @throws SQLException 
     */
    public void add(String message, int type, LocalDateTime date) throws SQLException {
        String sql = "INSERT INTO " + tableName + "\n"
                + "("
                + "message,"
                + "type,"
                + "date"
                + ") "
                + "VALUES (?, ?, ?);";
        PreparedStatement ps = null;
        Connection con = null;

        try {
            con = factory.createConnection();
            ps = con.prepareStatement(sql);

            ps.setString(1, message);
            ps.setInt(2, type);
            ps.setString(3, date.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (con != null) {
                con.close();
            }
        }
    }

    public void addToday(String message, int type) throws SQLException {
        add(message, type, LocalDateTime.now());
    }

    /**
     *
     * @param from
     * @param to
     * @return
     * @throws java.sql.SQLException
     */
    public List<EventDBO> getRange(LocalDate from, LocalDate to) throws SQLException {

        String sql = "SELECT * FROM " + tableName + "\n"
                + "WHERE date \n"
                + "BETWEEN ? \n"
                + "AND ? "
                + "ORDER BY date DESC";

        PreparedStatement statement = null;
        Connection con = null;
        List<EventDBO> resultsList = new LinkedList();
        
        try {
            con = factory.createConnection();
            statement = con.prepareStatement(sql);
            
            LocalDateTime fromTime = LocalDateTime.of(from, LocalTime.MIDNIGHT);
            LocalDateTime toTime = LocalDateTime.of(to, LocalTime.MAX);
            
            statement.setString(1, fromTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            statement.setString(2, toTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

            ResultSet set = statement.executeQuery();

            while (set.next()) {
                EventDBO tempEvent = new EventDBO();
                tempEvent.setID(set.getInt("ID"));
                tempEvent.setMessage(set.getString("message"));
                tempEvent.setType(set.getInt("type"));
                tempEvent.setDate(
                        LocalDateTime.parse(
                                set.getString("date"),
                                DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                );
                resultsList.add(tempEvent);
            }

        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }

        }

        return resultsList;

    }
    
    public List<EventDBO> getToday() throws SQLException {
        return getRange(LocalDate.now(), LocalDate.now());
    }

}
