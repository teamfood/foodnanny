/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gage
 */
public class Listdb {

    protected JDBCfactory jdbcFactory;
    protected String tableName;

    Listdb(String tableName, JDBCfactory jdbcFac) throws SQLException {
        this.tableName = tableName;
        this.jdbcFactory = jdbcFac;

        createTable();
    }

    Listdb(JDBCfactory jdbcFac) throws SQLException {
        this.tableName = "dummyList";
        this.jdbcFactory = jdbcFac;

        createTable();
    }

    Listdb() {

    }

    /**
     * This will create a table for this object if it doesn't exist.
     *
     * @throws SQLException
     */
    protected void createTable() throws SQLException {
        Connection con = jdbcFactory.createConnection();
        Statement statement = con.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " ("
                + "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "productID INTEGER,"
                + "dateAdded INTEGER"
                + ");";

        statement.execute(sql);
        statement.close();
        con.close();
    }

    /**
     * This will create a table for this object if it doesn't exist.
     *
     * @throws SQLException
     */
    public void deleteTable() throws SQLException {
        Statement statement = null;
        Connection con = null;

        try {
            con = jdbcFactory.createConnection();
            statement = con.createStatement();
            String sql = "DROP TABLE " + tableName;

            statement.execute(sql);
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }
        }
    }

    /**
     * This will add a product to the buy list.If the product already exists
     * (same ID) then it will add to the quantity
     *
     * @param productID
     * @param date
     * @return id of the new product
     * @throws java.sql.SQLException
     */
    public int add(int productID, long date) throws SQLException {

        String sql = "INSERT INTO " + tableName + ""
                + "(productID, dateAdded)"
                + "VALUES (?, ?);";

        PreparedStatement ps = null;
        Connection con = null;
        int newID = -1;
        try {
            con = jdbcFactory.createConnection();
            ps = con.prepareStatement(sql);

            ps.setInt(1, productID);
            ps.setLong(2, date);
            boolean resultSet = ps.execute();
            if (resultSet) {
                ResultSet results = ps.getResultSet();
                newID = results.getInt("ID");
            }

        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return newID;
    }

    /**
     * This is the same as above except it will default to today
     *
     * @param productID
     * @return
     * @throws SQLException
     */
    public int add(int productID) throws SQLException {
        Date today = new Date();
        return add(productID, today.getTime());
    }

    /**
     * This will remove
     *
     * @param productID
     * @param number you want to remove
     * @return number of items actually deleted
     * @throws java.sql.SQLException
     */
    public int remove(int productID, int number) throws SQLException {

        // Count home many there are first
        Statement countStatement = null;
        PreparedStatement statement = null;
        Connection con = null;
        int count = -1;

        try {
            con = jdbcFactory.createConnection();
            countStatement = con.createStatement();
            String countSQL = "SELECT COUNT(*) from " + tableName + "\n"
                    + "WHERE productID=" + productID + "";

            ResultSet executeQuery = countStatement.executeQuery(countSQL);
            executeQuery.next();

            count = executeQuery.getInt("COUNT(*)");

            countStatement.close();
            countStatement = null;

            // Delete statement
            String sql = "DELETE FROM " + tableName + " \n"
                    + "WHERE ID in \n"
                    + "(\n"
                    + "	SELECT dateAdded FROM " + tableName + "\n"
                    + "	WHERE productID=?\n"
                    + "	ORDER BY dateAdded ASC\n"
                    + "	LIMIT ?\n"
                    + ")";
            statement = con.prepareStatement(sql);

            statement.setInt(1, productID);
            statement.setInt(2, number);

            boolean execute = statement.execute();

        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (countStatement != null) {
                countStatement.close();
            }

            if (statement != null) {
                statement.close();
            }
        }

        return count <= number ? count : number;
    }

    /**
     * Get an object by ID
     *
     * @param ID of the buyList
     * @return
     * @throws SQLException
     */
    public Map<String, Object> get(int ID) throws SQLException {
        String sql = "SELECT * FROM " + tableName + "\n"
                + "WHERE ID=?;";

        PreparedStatement statement = null;
        Connection con = null;

        try {
            con = jdbcFactory.createConnection();
            statement = con.prepareStatement(sql);
            statement.setInt(1, ID);
            statement.execute();
            ResultSet set = statement.getResultSet();

            if (set == null) {
                return null;
            }

            set.next();

            HashMap hashSet = new HashMap();

            hashSet.put("ID", set.getInt("ID"));
            hashSet.put("productID", set.getInt("productID"));
            hashSet.put("dateAdded", new Date(set.getLong("dateAdded")));

            return hashSet;
        } catch (SQLException ex) {
            throw ex;
        } finally {

            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }
        }

    }

    /**
     * @return a "list" of buy list products in the database.
     * @throws java.sql.SQLException
     */
    public List<Map<String, Object>> getAllRaw() throws SQLException {
        String sql = "SELECT * FROM " + tableName;

        ResultSet set;
        List<Map<String, Object>> resultList = null;
        Statement statement = null;
        Connection con = null;

        try {
            con = jdbcFactory.createConnection();
            statement = con.createStatement();

            set = statement.executeQuery(sql);

            if (set == null) {
                return null;
            }

            resultList = new LinkedList<>();

            while (set.next()) {

                Map<String, Object> temp = new HashMap<>();
                temp.put("ID", set.getInt("ID"));
                temp.put("productID", set.getInt("productID"));
                temp.put("dateAdded", new Date(set.getLong("dateAdded")));
                resultList.add(temp);
            }

        } catch (SQLException ex) {
            Logger.getLogger(BuyListdb.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }
        }

        return resultList;

    }

    /**
     * @return a map by the productID which leads to maps(json like objects) of
     * each product with a qty added which is how many there are in the buylist.
     *
     * @throws java.sql.SQLException
     */
    public Map<Integer, ProductDBO> getAll() throws SQLException {
        String sql = "SELECT buy.ID, buy.dateAdded, "
                + "pt.ID as productID, pt.name, pt.daysGood, pt.barcode, "
                + "pt.wholesellerID, pt.maxQty, pt.Price \n"
                + "FROM " + tableName + " AS buy\n"
                + "INNER JOIN productType as pt\n"
                + "ON buy.ProductID=pt.ID";

        Statement statement = null;
        Connection con = null;
        Map<Integer, ProductDBO> resultMap = new HashMap<>();

        try {
            con = jdbcFactory.createConnection();
            statement = con.createStatement();

            ResultSet set = statement.executeQuery(sql);

            if (set == null) {
                return null;
            }

            while (set.next()) {

                ProductDBO temp = resultMap.get(set.getInt("productID"));

                if (temp == null) {

                    temp = new ProductDBO();
                    temp.setID(set.getInt("ID"));
                    temp.setProductTypeID(set.getInt("productID"));
                    temp.setDateAdded(new Date(set.getLong("dateAdded")));
                    temp.setName(set.getString("name"));
                    temp.setDaysGood(set.getInt("daysGood"));
                    temp.setBarcode(set.getString("barcode"));
                    temp.setWholesellerID(set.getInt("wholesellerID"));
                    temp.setPrice(set.getInt("Price"));
                    temp.setMaxQty(set.getInt("maxQty"));
                    temp.setQty(1);

                    resultMap.put(set.getInt("productID"), temp);
                } else {
                    temp.setQty(temp.getQty() + 1);
                }
            }

        } catch (SQLException ex) {
            throw ex;
        } finally {

            if (statement != null) {
                statement.close();
            }

            if (con != null) {
                con.close();
            }
        }

        return resultMap;

    }

}
