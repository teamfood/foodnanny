/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package foodNanny.db;

/**
 * This is a class that all Database Objects will derive from.
 * 
 * A database object will have NO ENCAPSOLATION. Meaning that all
 * the fields will be public. The reason for this is so we don't 
 * try to add logic to the getter's and setters and it's simply a
 * way to pass data.
 * 
 * @author Gage
 */
public abstract class DBO {
    
}
