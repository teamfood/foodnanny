/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * An interface to the e-mails database
 *
 * @author justgage
 */
public class Emailsdb {

    JDBCfactory factory;
    String tableName;

    public Emailsdb(JDBCfactory factory) throws SQLException {
        this.factory = factory;
        this.tableName = "email";
        
        createTable();
    }

    private void createTable() throws SQLException {
        Connection connection = factory.createConnection();
        Statement statement = connection.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " ("
                + "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "username TEXT,"
                + "password TEXT"
                + ");";

        statement.execute(sql);
    }

    public List<EmailDBO> getAll() {

        return null;
    }

    /**
     * Get an object by ID
     *
     * @param ID of the buyList
     * @return
     * @throws SQLException
     */
    public EmailDBO get(int ID) throws SQLException {
        String sql = "SELECT * FROM " + tableName + "\n"
                + "WHERE ID=?;";
        Connection connection = factory.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, ID);
        ResultSet set = statement.getResultSet();

        // if the result set is empty return null
        if (set == null || !set.isBeforeFirst()) {
            return null;
        }

        EmailDBO email = new EmailDBO(set.getInt("ID"),
                set.getString("username"),
                set.getString("password"));
        
        return email;

    }
    
    /**
     * Add an EmailDBO to the database
     * @param email
     */
    public void add(EmailDBO email) {
        
    }

}
