/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package foodNanny.app;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;

/**
 * Creates Notifications to be displayed in the Listviews in different GuI
 * windows.
 * @author Jeff
 */
public class Notification {
    
    //Declare the variables
    private String productBarcode;
    private int productQuantity;
    private String productName;
    private int productRemoved;
    private GridPane grid;
    private Label labelName;
    private Label labelQuantity;
    private Label labelBarcode;
 
    /**
     * Returns a removed product.
     * @return A removed product.
     */
    public int getProductRemoved() {
        return productRemoved;
    }

    /**
     * Sets which product is removed.
     * @param productRemoved Takes in a product removed as an Integer.
     */
    public void setProductRemoved(int productRemoved) {
        this.productRemoved = productRemoved;
    }
    
    /**
     * Returns a product's bar code.
     * @return A product bar code
     */
    public String getProductBarcode() {
        return productBarcode;
    }

    /**
     * Sets the product's bar code, and sets the label as well.
     * @param productBarcode Takes in a product bar code as a String
     */
    public void setProductBarcode(String productBarcode) {
        this.productBarcode = productBarcode;
        labelBarcode.setText("Barcode: " + productBarcode);
    }

    /**
     * Returns a product's quantity.
     * @return The quantity of a product
     */
    public int getProductQuantity() {
        return productQuantity;
    }

    /**
     * Sets the product's quantity, and sets the label as well.
     * @param productQuantity Takes in a product quantity as an Integer.
     */
    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
        labelQuantity.setText(Integer.toString(productQuantity) + "x ");
    }

    /**
     * Returns the product's name.
     * @return The product's name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the product's name, and sets the label as well.
     * @param productName Takes in a product name as a String.
     */
    public void setProductName(String productName) {
        this.productName = productName;
        labelName.setText(productName);
    }

    /**
     * Returns the product's Gridpane.
     * @return The product's Gridpane
     */
    public GridPane getGrid() {
        return grid;
    }
    
    /**
     * Constructs a new notification through the appropriate product parameters.
     * @param qty Takes in a quantity as an Integer
     * @param name Takes in a name as a String
     * @param barcode Takes in a bar code as a String
     * @param totalRemoved Takes in a quantity removed as an Integer
     */
    public Notification(int qty, String name, String barcode, int totalRemoved) {
        
        //Instantiate the variables.
        grid = new GridPane();
        this.productQuantity = qty;
        this.productName = name;
        this.productBarcode = barcode;
        this.productRemoved = totalRemoved;
        labelName = new Label();
        
        //Style the notification
        labelName.setFont(new Font("Calibri", 20));
        labelQuantity = new Label();
        labelQuantity.setFont(new Font("Calibri", 20));
        labelBarcode = new Label(getProductBarcode());
        labelBarcode.setFont(new Font("Calibri", 12));       
        
        //Set the product up
        setProductName(productName);
        setProductQuantity(productQuantity);
        setProductBarcode(productBarcode);
        
        //Add the product to the Gridpane
        grid.add(labelName, 1, 0);
        grid.add(labelQuantity, 0, 0);
        grid.add(labelBarcode, 1, 1, 10, 1);
    }
}
