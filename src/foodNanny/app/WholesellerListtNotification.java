/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.app;

import foodNanny.db.BuyListdb;
import foodNanny.db.Emailsdb;
import foodNanny.db.Eventdb;
import foodNanny.db.JDBCfactory;
import foodNanny.db.ProductDBO;
import foodNanny.db.Settings;
import foodNanny.db.SettingsDBO;
import foodNanny.db.StockListdb;
import foodNanny.db.WholesellerDBO;
import foodNanny.db.Wholesellerdb;
import foodNanny.ui.InventoryList;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.mail.MessagingException;

/**
 *
 * @author har09_000
 */
public class WholesellerListtNotification {

    private int productWholeSellerID;
    private String productWholeSeller;
    private Button buttonEditWholeSellerInfo;
    private ListView<GridPane> products;
    private GridPane grid;
    private Label labelWholeSeller;
    private int listSize;

    private String emailText;
    private final Button buttonSend;
    List<ProductDBO> productsDBO;

    public String getEmailText() {
        return emailText;
    }

    public void setEmailText(String text) {
        this.emailText = text;
    }

    public GridPane getGrid() {
        return grid;
    }

    public int getProductWholeSellerID() {
        return productWholeSellerID;
    }

    public void setProductWholeSellerID(int productWholeSellerID) {
        this.productWholeSellerID = productWholeSellerID;
    }

    public String getProductWholesellerName() {
        return productWholeSeller;
    }

    public void setProductWholesellerName(String productWholeSeller) {
        this.productWholeSeller = productWholeSeller;
        labelWholeSeller.setText(productWholeSeller);
    }

    public WholesellerListtNotification(WholesellerDBO wholeSeller) {
        productsDBO = new LinkedList<>();
        JDBCfactory factory = new JDBCfactory();
        grid = new GridPane();
        //System.out.println("NAME: " + wholeSeller.getName());
        labelWholeSeller = new Label();

        setProductWholesellerName(wholeSeller.getName());
        setProductWholeSellerID(wholeSeller.getId());
        setEmailText(wholeSeller.getMessage());

        labelWholeSeller.setFont(new Font("Calibri", 28));
        // doesn't work yet GridPane.setHgrow(labelWholeSeller, Priority.ALWAYS);

        buttonSend = new Button("Send");
        buttonSend.setOnAction((ActionEvent ActionEvent) -> {
            try {
                Emailsdb emailsdb = new Emailsdb(factory);
            } catch (SQLException ex) {
                Logger.getLogger(WholesellerListtNotification.class.getName()).log(Level.SEVERE, null, ex);
            }
            final Stage stage = new Stage();
            GridPane grid1 = new GridPane();
            grid1.setAlignment(Pos.CENTER);
            grid1.setHgap(10);
            grid1.setVgap(10);
            grid1.setPadding(new Insets(25, 25, 25, 25));

            Label wholesellerLabel = new Label("Wholesaler Name: " + wholeSeller.getName());
            Label emailLabel = new Label("Wholesaler Email: " + wholeSeller.getEmail());
            Label messageLabel = new Label("Default Message");
            Label errorLabel = new Label("");

            TextArea textArea = new TextArea(wholeSeller.getMessage());

            textArea.setMinWidth(300);
            WebView browser = new WebView();
            WebEngine webEngine = browser.getEngine();
            SettingsDBO settings;
            Gmail gmail;
            Button send = new Button("Send");
            Button cancel = new Button("Cancel");

            try {
                settings = new Settings().getTheSettings();

                gmail = new Gmail(settings.getEmailUsername(), settings.getEmailPassword());
                send.setOnAction((ActionEvent event) -> {
                    try {
                        gmail.sendProductList(wholeSeller.getEmail(), textArea.getText(), productsDBO);
                        try {
                            Eventdb sendEvent = new Eventdb(factory);
                            String mess = "BuyList Sent off To: " + wholeSeller.getName() + "\n";
                            double total = 0;
                            for (ProductDBO entry : productsDBO) {
                                Double subtotal =  entry.getPrice() * entry.getQty();
                                mess +=" " + entry.getQty() +
                                        " of " + entry.getName() 
                                        + " = $ "  + String.format("%.2f", subtotal)
                                        + "\n";
                                
                                total += subtotal;
                            }
                            
                            mess += "\t TOTAL: $ " + String.format("%.2f", total);
                            sendEvent.addToday(mess, 3);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }

                        stage.close();
                    } catch (MessagingException ex) {
                        send.setText("Send");
                        errorLabel.setText("ERROR sending message!\n (" + ex + ")");
                    }
                });
                webEngine.loadContent(gmail.generateHTML(productsDBO), "text/html");
            } catch (Exception ex) {
                Logger.getLogger(WholesellerListtNotification.class.getName()).log(Level.SEVERE, null, ex);
            }

            send.setFont(Font.font(18));
            cancel.setFont(Font.font(18));
            
            cancel.setOnAction((ActionEvent event) -> {
                stage.close();
            });

            GridPane.setFillHeight(browser, true);
            GridPane.setFillWidth(browser, true);
            GridPane.setHalignment(send, HPos.RIGHT);
            grid1.add(wholesellerLabel, 0, 0, 1, 1);
            grid1.add(emailLabel, 0, 1, 1, 1);
            grid1.add(messageLabel, 0, 2, 1, 1);
            grid1.add(textArea, 0, 3, 1, 1);
            grid1.add(browser, 1, 0, 1, 4);
            grid1.add(errorLabel, 0, 4, 2, 1);
            grid1.add(cancel, 0, 5);
            grid1.add(send, 1, 5);
            Scene scene = new Scene(grid1, 800, 400);
            stage.setScene(scene);
            stage.show();

        });

        buttonEditWholeSellerInfo = new Button("Edit");
        buttonEditWholeSellerInfo.setOnAction((ActionEvent event) -> {
            final Stage stage = new Stage();
            GridPane grid1 = new GridPane();
            grid1.setAlignment(Pos.CENTER);
            grid1.setHgap(10);
            grid1.setVgap(10);
            grid1.setPadding(new Insets(25, 25, 25, 25));
            Label wholesellerLabel = new Label("Wholesaler Name");
            Label emailLabel = new Label("Wholesaler Email");
            Label messageLabel = new Label("Default Message");
            TextField editWhole = new TextField();
            editWhole.setText(wholeSeller.getName());
            TextField editEmail = new TextField();
            editEmail.setText(wholeSeller.getEmail());
            TextArea message = new TextArea();
            message.setText(wholeSeller.getMessage());
            System.out.println("NAME: " + wholeSeller.getName());
            System.out.println("EMAIL: " + wholeSeller.getEmail());
            System.out.println("MESSAGE: " + wholeSeller.getMessage());

            Button OK = new Button("OK");
            OK.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        Wholesellerdb wdb = new Wholesellerdb(factory);

                        wholeSeller.setName(editWhole.getText());
                        wholeSeller.setEmail(editEmail.getText());
                        wholeSeller.setMessage(message.getText());
                        setProductWholesellerName(editWhole.getText());
                        wdb.update(wholeSeller);
                        System.out.println("NAME: " + wholeSeller.getName());
                        System.out.println("EMAIL: " + wholeSeller.getEmail());
                        System.out.println("MESSAGE: " + wholeSeller.getMessage());

                        stage.close();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            });
            grid1.add(wholesellerLabel, 0, 0);
            grid1.add(editWhole, 0, 1);
            grid1.add(emailLabel, 0, 2);
            grid1.add(editEmail, 0, 3);
            grid1.add(messageLabel, 0, 4);
            grid1.add(message, 0, 5);
            grid1.add(OK, 0, 6);
            Scene scene = new Scene(grid1, 400, 400);
            stage.setScene(scene);
            stage.show();
        });
        products = new ListView<>();

        try {
            BuyListdb stock = new BuyListdb(factory);
            Map<Integer, ProductDBO> currentStock = stock.getAll();

            for (ProductDBO entry : currentStock.values()) {
                String barcode = entry.getBarcode();
                String name = entry.getName();
                int wholeID = entry.getWholesellerID();
                int qty = entry.getQty();
                int tRemoved = entry.getQtyRemoved();
                //System.out.println(wholeID);

                if (getProductWholeSellerID() == wholeID) {
                    Notification notification = new Notification(qty, name, barcode, tRemoved);
                    listSize += 1;
                    System.out.println(listSize);
                    products.getItems().add(notification.getGrid());
                    productsDBO.add(entry);
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(InventoryList.class.getName()).log(Level.SEVERE, null, ex);
        }

        final int ROW_HEIGHT = 24;
        products.setPrefHeight(listSize * ROW_HEIGHT + (33 * listSize));
        products.setMaxWidth(225);

        grid.setHgap(5);
        grid.setVgap(5);
        grid.setPadding(new Insets(0, 0, 0, 0));

        grid.add(labelWholeSeller, 0, 0, 8, 1);
        grid.add(buttonEditWholeSellerInfo, 9, 0, 1, 1);
        grid.add(buttonSend, 10, 0, 1, 1);
        grid.add(products, 0, 1, 20, 1);
    }

}
