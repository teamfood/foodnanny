/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.app;

import foodNanny.db.EventDBO;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * Creates an event notification to be used on the main page to show recent
 * events, as well as events for the History Page.
 * @author Jeff
 */
public class EventNotification {

    //Declare the variables
    private GridPane grid;
    private String message;
    private LocalDateTime date;
    private Label messageLabel;
    private Label timeLabel;
    private int type;

    /**
     * Returns the Event message.
     * @return An event message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the Events message text
     * @param message Takes in a message as a String.
     */
    public void setMessage(String message) {
        this.message = message;
        messageLabel.setText(message);
    }

    /**
     * Returns an event type.
     * @return an event type.
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the events type.
     * @param type Takes in an event type as an Integer.
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Returns an event grid.
     * @return an event grid
     */
    public GridPane getGrid() {
        return grid;
    }

    /**
     * Returns an event's date.
     * @return An event's date
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Sets an events date text.
     * @param date Takes in a date as a LocalDateTime
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
        timeLabel.setText(date.format(DateTimeFormatter.ofPattern("E MMM d h:ma")));
    }

    /**
     * Constructs an Event from the parameters held in the EventDBO object.
     * @param event 
     */
    public EventNotification(EventDBO event) {

        //Instantiate the variables.
        grid = new GridPane();
        this.messageLabel = new Label();
        this.timeLabel = new Label();

        //Set up the grid pane.
        GridPane.setHgrow(messageLabel, Priority.ALWAYS);
        GridPane.setHalignment(timeLabel, HPos.RIGHT);
        GridPane.setValignment(timeLabel, VPos.TOP);

        //Set up the event from the EventDBO
        setMessage(event.getMessage());
        setType(event.getType());
        setDate(event.getDate());

        //Fill the grid pane
        grid.add(messageLabel, 0, 0);
        grid.add(timeLabel, 1, 0);

        //Style the grid pane
        grid.setStyle(""
                + "-fx-background-color: #EEE; \n"
                + "-fx-background-insets: 0, 1px ;\n"
                + "-fx-background-radius: 4px, 0 ;"
                + "-fx-padding: 10px;"
        );

    }

}
