/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.app;

import foodNanny.ui.Add;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;

/**
 * Provides a template for creating Gridpanes
 *
 * @author Jeff
 */
public class GridPaneCreator {

    //Declare the variables.
    Add currentList = new Add();
    private Label productName;
    private Label quantity;
    private Label tempBarcode;
    public ListView<GridPane> list;
    GridPane grid;

    /**
     * Takes in a product name, quantity, and bar code to create a new Gridpane.
     *
     * @param name Takes in a product name as a String
     * @param qty Takes in a product quantity as an Integer
     * @param barcode Takes in a product bar code as an Integer
     */
    public GridPaneCreator(String name, int qty, int barcode) {

        //Instantiate the variables
        grid = new GridPane();
        productName = new Label();
        quantity = new Label();
        tempBarcode = new Label();

        list = currentList.getAddList();
        if (list.getItems().contains(grid)) {
        }

        //Style the grid pane
        productName.setText(name);
        productName.setFont(new Font("Calibri", 35));
        quantity.setText("" + qty);
        quantity.setFont(new Font("Calibri", 35));
        tempBarcode.setText("" + barcode);

        //Fill the grid pane
        grid.add(productName, 0, 0);
        grid.add(quantity, 1, 0);
        grid.add(tempBarcode, 0, 1);

    }
}
