/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.app;

import foodNanny.db.ProductDBO;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author justgage
 */
public class Gmail {

    private String username;
    private String password;

    /**
     * Create a gmail object
     *
     * @param username gmail username WITHOUT the @gmail.com
     * @param password gmail password
     */
    public Gmail(String username, String password) {
        this.username = username;
        this.password = password;

    }

    /**
     * @deprecated This is just for testing
     */
    public Gmail() {
        this.username = "gkpbiz";
        this.password = "rM903eJjFBiB";

    }

    public void send(String[] to, String subject, String body) throws AddressException, MessagingException {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", username);
        props.put("mail.smtp.password", password);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        message.setFrom(new InternetAddress(username));
        InternetAddress[] toAddress = new InternetAddress[to.length];

        // To get the array of addresses
        for (int i = 0; i < to.length; i++) {
            toAddress[i] = new InternetAddress(to[i]);
        }

        for (int i = 0; i < toAddress.length; i++) {
            message.addRecipient(Message.RecipientType.TO, toAddress[i]);
        }

        message.setSubject(subject);
        message.setContent(body, "text/html");
        Transport transport = session.getTransport("smtp");
        transport.connect(host, username, password);
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }

    /**
     * Same as send except emails are are comma seperated string
     *
     * @param toEmails
     * @param subject
     * @param body
     * @throws MessagingException
     */
    public void send(String toEmails, String subject, String body) throws MessagingException {
        String[] to = toEmails.split(",");
        send(to, subject, body);
    }

    public void sendProductList(String toEmails, String comments, List<ProductDBO> list) throws MessagingException {

        String body = comments.replace("\n", "<br />") + "<br />" + generateHTML(list);

        send(toEmails, "Order From " + "" + "", body);

    }

    public String generateHTML(List<ProductDBO> list) {
        String tdStyle="<td style=\""
                + "border-bottom:1px solid #555;"
                + "padding: 10px;"
                + "font-family:georgia,'times',serif;"
                + "\">";
        String thStyle="<th style=\""
                + "border-bottom:2px solid #555;"
                + "padding: 10px;"
                + "font-family:georgia,'times',serif;"
                + "\">";
        
        String html = ""
                + "<br />"
                + "<table style=\"text-align:center;\" title=\"Products Statement\">"
                + "<thead style=\"font-family:georgia,'times',serif;\">"
                + "<tr>"
                + thStyle
                + "Name"
                + "</th>"
                + thStyle
                + "Quantity"
                + "</th>"
                + thStyle
                + "Price"
                + "</th>"
                + thStyle
                + "Subtotal"
                + "</th>"
                + "</tr>"
                + "</thead>";

        double total = 0;
        
        for (ProductDBO entry : list) {
            
            Double subtotal =  entry.getPrice() * entry.getQty();
            
            html += "<tr>"
                    + tdStyle
                    + entry.getName()
                    + "</td>"
                    +  tdStyle
                    + entry.getQty()
                    + "</td>"
                    +  tdStyle
                    + String.format("%.2f", entry.getPrice())
                    + "</td>"
                    +  tdStyle
                    + "<strong>$"+ String.format("%.2f",subtotal) + "</strong>"
                    + "</td>"
                    + "</tr>";
            
            total += subtotal;
        }
        
        html += "</table>";
        
        html += "<br /><h2 style=\"font-family:georgia,'times',serif;\">Expected Total: $" + String.format("%.2f", total) + "</h2>";

        return html;
    }
}
