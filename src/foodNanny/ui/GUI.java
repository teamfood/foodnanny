/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package foodNanny.ui;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;

public abstract class GUI {

//scene variable used to send this GUI to primary stage
    private Scene guiScene;

    //list of contents in the top-level container of the GUI
    ObservableList<Node> guiContents;
    
    //identical top menu-bar options for each GUI

    /**
     * Constructor for all GUIs - 
     * Initializes then lays out the contents of the GUI perspective
     */
    public GUI() {
    	initialize();
    	layout();
    }

    /**
     * Creates and initializes all UI elements for this perspective
     */
    protected void initialize() {
    	//set title
        Main.getStage().setTitle(getTitle());

    }

    /**
     * Lays out and places all UI elements within the scene
     */
    protected void layout() {
	//All GUIs will have the menuBar at the top, everything else inside
	VBox vbox = new VBox();

        guiContents = vbox.getChildren();

	//set scene using the top-level container
	guiScene = new Scene(vbox);
        //guiScene.getStylesheets().add("ui/style.css");
    }

    /**
     * Used to add content to the top-level container in the GUI
     * @param item The item to be added
     */
    protected void addToGUI(Node item) {
    	guiContents.add(item);
    }

    /**
     * Prompts the user if they are sure they want to change perspectives,
     * then changes perspectives if they answer "Yes"
     * @param perspective The perspective to change to
     */
    public void checkThenChange(final GUI perspective, String phrase) {
	Runnable change = new Runnable() {
            @Override
        	public void run() {
                    Main.getInstance().changePerspective(perspective);
                }
            };

        YesNoDialogue prompt = new YesNoDialogue(phrase, change, null);

        //tell the application to launch this prompt, replacing any previous prompt
        Main.getInstance().dialogue(prompt.getStage());
    }

    /**
     * Gets the Scene containing this GUI perspective
     * @return The scene containing this GUI perspective
     */
    public Scene getScene() {
    	return guiScene;
    }

    /**
     * Get the title of this perspective
     * @return The title
     */
    public static String getTitle() {
    	return "GUI";
    }

}
