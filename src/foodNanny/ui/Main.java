/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * I changed something. My name to the top comment or something.
 * Added my name:  Marc Hartley
 */

package foodNanny.ui;

import foodNanny.db.BuyListdb;
import foodNanny.db.JDBCfactory;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {
    //reference to only GUI instance (singleton pattern)
    private static Main instance;
    private static GUI gui;
    private static Stage stage;

    //current dialogue window, if there is one
    private Stage dialogue;

    /**
     * Starts and displays the Quiz Generator application GUI
     * @throws java.lang.Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
    	
        //set this to be the only GUI instance
        instance = this;
	stage = primaryStage;
        
	stage.setTitle("Food Nanny");
	stage.setResizable(false);
        stage.setWidth(600);
        stage.setHeight(600);

	//initial perspective is the start menu
	changePerspective(new MainMenu());

	stage.show();
    }

    /**
     * Changes the perspective of the GUI between the various tools the program provides
     * @param perspective The perspective to change to
     */
    public void changePerspective(GUI perspective) {
	gui = perspective;
	stage.setScene(perspective.getScene());
    }

    /**
     * Gets the current GUI perspective
     * @return The current GUI perspective
     */
    public static GUI getPerspective() {
        return gui;
    }

    /**
     * Displays a dialogue pop-up window, only allows one pop-up to be open at a time
     * @param window The popup window to display
     */
    public void dialogue(Stage window) {
    	//terminate existing dialogue, if there is one
    	if(dialogue != null) {
            dialogue.close();
        }

	dialogue = window;
	dialogue.show();
    }

    /**
     * Get the singleton instance of this application
     * @return The only instance of this application
     */
    public static Main getInstance() {
	return instance;
    }

    /**
     * Get the primary application stage
     * @return The primary application stage
     */
    public static Stage getStage() {
    	return stage;
    }
    
    public static void main(String[] args) {
	launch(args);
    }
}