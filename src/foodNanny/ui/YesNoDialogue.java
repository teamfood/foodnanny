/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Used as a template to create Yes or No Dialogue boxes.
 *
 * @author Jeff
 */
public class YesNoDialogue {

    //Declare the member variables
    private Stage stage;
    private String prompt;
    private Runnable yesAction;
    private Runnable noAction;

    /**
     * Constructs a YesNoPrompt dialogue window
     *
     * @param prompt The query String to display
     * @param yesAction The action to take if "yes" is selected
     * @param noAction The action to take if "no" is selected
     */
    public YesNoDialogue(String prompt, Runnable yesAction, Runnable noAction) {
        this.prompt = prompt;
        this.yesAction = yesAction;
        this.noAction = noAction;
        setupStage();
    }

    /**
     * Sets up the GUI, as well as handles the events
     */
    private void setupStage() {

        //Instantiate the variables
        stage = new Stage();
        GridPane grid = new GridPane();
        Label promptLabel = new Label(prompt);
        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");

        //Set up the grid
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        //Handle the yes click
        yesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //if "yes" button is pressed, set answer to true
                if (yesAction != null) {
                    yesAction.run();
                }
                //close dialogue window
                stage.close();
            }
        });

        //Handle the no click
        noButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //if "no" button is pressed, set answer to false
                if (noAction != null) {
                    noAction.run();
                }
                stage.close();
            }
        });
        
        //Fill in the grid
        grid.add(promptLabel, 0, 0, 3, 1);
        grid.add(noButton, 2, 1);
        grid.add(yesButton, 1, 1);

        stage.setScene(new Scene(grid));
    }

    /**
     * Get this YesNoPrompt as a Stage
     *
     * @return The dialogue window
     */
    public Stage getStage() {
        return stage;
    }
}
