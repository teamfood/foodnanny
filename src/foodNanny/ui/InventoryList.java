/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.ui;

import foodNanny.app.WholesellerListtNotification;
import foodNanny.app.Notification;
import foodNanny.db.JDBCfactory;
import foodNanny.db.ProductDBO;
import foodNanny.db.StockListdb;
import foodNanny.db.WholesellerDBO;
import foodNanny.db.Wholesellerdb;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Generates an Inventory List GUI that holds stock list, buy list, and some
 * wholesaler information for viewing and sending orders, as well as editing
 * final changes to wholesaler and email information.
 *
 * @author Jeff
 */
public class InventoryList extends GUI {

    //Declare the member variables
    private float totalMoney;
    private Button goBack;
    private Button sendOrders;
    private Label stockListLabel;
    private Label buyListLabel;
    private Label moneySavedLabel;
    private Label moneySavedAmount;
    private static ListView<GridPane> stockList;
    private static ListView<GridPane> buyList;

    //Set the icon location for the Go Back button
    private static final String GO_BACK
            = "http://icons.iconarchive.com/icons/custom-icon-design/flatastic-1/24/back-icon.png";

    /**
     * Acts as the main method for the Inventory List class in defining
     * variables and handling events.
     */
    @Override
    protected void initialize() {
        Main.getStage().setTitle(getTitle());
        final Main app = Main.getInstance();

        //Instantiate the variables
        JDBCfactory factory = new JDBCfactory();
        moneySavedLabel = new Label();
        moneySavedAmount = new Label();
        stockList = new ListView();
        buyList = new ListView();
        moneySavedLabel.setText("Saved:");
        moneySavedAmount.setText("$" + totalMoney);
        stockListLabel = new Label("Stock List");
        buyListLabel = new Label("Buy List");

        //Style the GUI
        moneySavedAmount.setTextFill(Color.LAWNGREEN);
        moneySavedLabel.setFont(new Font("Calibri", 20));
        moneySavedAmount.setFont(new Font("Calibri", 20));
        stockListLabel.setFont(new Font("Calibri", 30));
        buyListLabel.setFont(new Font("Calibri", 30));
        stockList.setStyle("-fx-border-color: black");
        buyList.setStyle("-fx-border-color: black");

        //Set up the send orders button
        sendOrders = new Button("Send Orders");
        sendOrders.setStyle("-fx-font: 16 calibri");
        sendOrders.setMaxWidth(120);
        sendOrders.setMaxHeight(220);

        buyList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        stockList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        //Set up the image for the Go Back Button
        Image goBackImage = new Image(GO_BACK);
        Node backImage = new ImageView(goBackImage);

        //Create and fill the go back button
        goBack = new Button("Back", backImage);

        //Make the Go Back button take the user back to the main page
        goBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GUI back = new MainMenu();
                app.changePerspective(back);

            }
        });

        //Display the stock list
        try {
            StockListdb stock = new StockListdb(factory);
            Map<Integer, ProductDBO> currentStock = stock.getAll();
            for (ProductDBO entry : currentStock.values()) {
                //Get the values out of the current stock list
                String barcode = entry.getBarcode();
                String name = entry.getName();
                int qty = entry.getQty();
                int tRemoved = entry.getQtyRemoved();

                //Create notifications with the Stock list
                Notification notification = new Notification(qty, name, barcode, tRemoved);

                //Add the notifications to the grid pane
                stockList.getItems().add(notification.getGrid());
            }

            //Set up the wholesaler
            Wholesellerdb buy = new Wholesellerdb(factory);
            List<WholesellerDBO> all = buy.getAllShallow();

            //Display the buy list
            for (WholesellerDBO entry : all) {
                WholesellerListtNotification buyListNotification = new WholesellerListtNotification(entry);

                //Add the notifications to the grid pane
                buyList.getItems().add(buyListNotification.getGrid());
            }

        } catch (SQLException ex) {
            System.out.println("Failure" + ex);
        }

        sendOrders.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            }
        });
    }

    /**
     * Sets up the Gridpane layout, as well as horizontal and vertical gap, and
     * padding.
     */
    @Override
    protected void layout() {
        super.layout();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(5);
        grid.setVgap(5);
        grid.setPadding(new Insets(10, 10, 10, 10));

        grid.add(moneySavedLabel, 90, 0, 110, 1);
        grid.add(moneySavedAmount, 105, 0, 120, 1);
        grid.add(stockList, 3, 6, 50, 50);
        grid.add(buyList, 64, 6, 50, 50);
        grid.add(goBack, 0, 0, 50, 1);
        grid.add(stockListLabel, 15, 5, 70, 1);
        grid.add(buyListLabel, 78, 5, 120, 1);
        grid.add(sendOrders, 44, 56, 30, 15);

        this.addToGUI(grid);

    }

    /**
     * Gets the title of this perspective (used to display title at the top of
     * the page and to display the name of the perspective in the "change
     * perspective" menu)
     *
     * @return The title of this perspective
     */
    public static String getTitle() {
        return "Inventory List";
    }

}
