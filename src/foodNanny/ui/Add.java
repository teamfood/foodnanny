/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.ui;

import foodNanny.app.Notification;
import foodNanny.db.Eventdb;
import foodNanny.db.JDBCfactory;
import foodNanny.db.ProductDBO;
import foodNanny.db.ProductTypesdb;
import foodNanny.db.StockListdb;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author Jeff
 */
public class Add extends GUI {

    //Declare the gui elements
    private Button cancelButton;
    private Button saveButton;
    private Button addButton;
    private Button addProductType;
    private Text labelOne;
    private Text labelTwo;
    private TextField addField;
    private TextField quantityField;
    private ListView<GridPane> addList;

    //Declare list storage and product types
    private ProductTypesdb productType;
    private Map<String, Notification> tempProducts;

    //Set the icon locations for the buttons.
    private static final String ADD_ICON
            = "http://icons.iconarchive.com/icons/awicons/vista-artistic/24/add-icon.png";

    private static final String CANCEL
            = "http://icons.iconarchive.com/icons/hopstarter/sleek-xp-basic/24/Close-icon.png";

    private static final String OKAY
            = "http://icons.iconarchive.com/icons/oxygen-icons.org/oxygen/24/Actions-dialog-ok-apply-icon.png";

    /**
     * *************************************************************************
     * GET ADD LIST Return the add list
     * ***********************************************************************
     */
    public ListView<GridPane> getAddList() {
        return addList;
    }

    /**
     * GET TITLE Return the GUI window title
     *
     * @return
     */
    public static String getTitle() {
        return "Add to Inventory";
    }

    /**
     * 
     * Initialize is the main class of Add defining variables and
     * handling action events.
     * 
     */
    @Override
    protected void initialize() {

        Main.getStage().setTitle(getTitle());
        final Main app = Main.getInstance();

        //Instantiate the public member variables
        JDBCfactory factory = new JDBCfactory();
        try {
            productType = new ProductTypesdb(factory);
            tempProducts = new HashMap();
            addProductType = new Button("New Product Type");
            addButton = new Button("Add");
            labelOne = new Text("Items to Add");
            labelTwo = new Text("Scan Items to add to the list");
            addList = new ListView();
            addField = new TextField();
            quantityField = new TextField();
            addProductType.setMaxHeight(50);

            //Action handler for adding a new product type
            addProductType.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    ProductTypeManager ptm = new ProductTypeManager();
                    ptm.createView();
                }
            });

            //Set a border for the add list
            addList.setStyle("-fx-border-color: black");

            //Set up the labels
            labelOne.setFont(new Font("Verdana", 35));
            labelTwo.setFont(Font.font("Verdana", FontWeight.BOLD, 13));

            //Set up the quantity field size and default text
            quantityField.setMaxWidth(40);
            quantityField.setPromptText("Qty");

            //Set the add field default text
            addField.setPromptText("Barcode Number");

            //Set up the image for the cancel button
            Image cancelButtonImage = new Image(CANCEL);
            Node cancelImage = new ImageView(cancelButtonImage);

            //Fill in the cancel button
            cancelButton = new Button(" Cancel", cancelImage);

            //Set cancel button bounds
            cancelButton.setMaxWidth(100);
            cancelButton.setMaxHeight(50);

            //Set up the image for the add button
            Image stockListNewProductImage = new Image(ADD_ICON);
            Node stockListNewProduct = new ImageView(stockListNewProductImage);

            //Create and style the add button
            addButton = new Button(" Add", stockListNewProduct);
            addButton.setStyle("-fx-font: 12 calibri");

            //Set the add button bounds
            addButton.setMaxWidth(100);
            addButton.setMaxHeight(60);

            //Set up the image for the save button
            Image saveButtonImage = new Image(OKAY);
            Node saveImage = new ImageView(saveButtonImage);

            //Fill in the save button
            saveButton = new Button(" Save", saveImage);

            //Set the save button bounds
            saveButton.setMaxWidth(100);
            saveButton.setMaxHeight(50);

            //AddField handler for pressing enter on the add field
            addField.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    addChange();
                }
            });

            //Cancel button handler for canceling
            cancelButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    GUI cancel = new MainMenu();

                    //Cancel if no changes have been made or prompt for user decision
                    if (addList.getItems().isEmpty()) {
                        app.changePerspective(cancel);
                    } else {
                        cancel.checkThenChange(cancel, "You have not saved your current additions, "
                                + "are you sure you want to cancel");
                    }
                }
            });

            //Save Button event handler for saving the added items to the stock
            saveButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    JDBCfactory factory = new JDBCfactory();
                    ProductDBO productDBO = new ProductDBO();
                    try {
                        StockListdb stockList = new StockListdb(factory);

                        String ProductNames = "";
                        boolean first = true;
                        //Loop through the temporary products list for values to store
                        for (Map.Entry<String, Notification> entry : tempProducts.entrySet()) {
                            //Get the values from the hash map to be stored in the product DBO
                            String barcodeKey = entry.getKey();
                            Notification value = entry.getValue();
                            int qty = value.getProductQuantity();
                            String name = value.getProductName();

                            //Set the product DBO values and stocklist quantity
                            stockList.add(barcodeKey, qty);
                            productDBO.setBarcode(barcodeKey);
                            productDBO.setName(name);
                            productDBO.setQty(qty);
                            productDBO.setQtyRemoved(0);

                            if (first) {
                                first = false;
                            } else {
                                ProductNames +="\n";
                            }

                            ProductNames += "   " + qty + " of " + name;

                        }
                        Eventdb events = new Eventdb(factory);
                        events.addToday("Added \n" + ProductNames, 1);
                        //Go back to main menu
                        GUI save = new MainMenu();
                        app.changePerspective(save);
                    } catch (SQLException ex) {
                        System.out.println("Unable to save.");
                    }
                }
            });

            //Add button handler for adding products to be used in stock list
            addButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    addChange();
                }
            });

        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
    }

    /**
     * 
     * This method is called from the add field and add change event
     * handlers. It takes the information from the quantity field and add field
     * to add the product to the temporary list.
     * 
     */
    public void addChange() {
        try {
            int qty;
            String barcode;

            //Default quantity to 1
            if (quantityField.getText().equals("")) {
                qty = 1;
            } else {
                qty = Integer.parseInt(quantityField.getText());
                if (qty == 0) {
                    qty = 1;
                }
            }

            //Get the barcode from the add field
            barcode = addField.getText();

            //Create a new notification if product exists
            if (tempProducts.containsKey(barcode)) {
                Notification n = tempProducts.get(barcode);
                n.setProductQuantity(n.getProductQuantity() + qty);
            } //If the product doesn't exist, add it to the database
            else {
                ProductDBO product = productType.getByBarcode(barcode);
                Notification notification = new Notification(qty, product.getName(), barcode, 0);
                tempProducts.put(barcode, notification);
                addList.getItems().add(notification.getGrid());
            }

            //Clear the fields and reset cursor to add field after adding
            addField.clear();
            addField.requestFocus();

        } catch (Exception e) {
            Error error = new Error();
            error.createView("Product type " + addField.getText() + " does not exist");
        }
    }

    /**
     * 
     * This method handles setting up the GUI window by adding in the gui
     * objects, as well as setting alignment and padding.
     * 
     */
    @Override
    protected void layout() {
        super.layout();

        //Create the grid pane and set the padding, gaps, and alignment
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(5);
        grid.setVgap(5);
        grid.setPadding(new Insets(10, 10, 10, 10));

        //Add the GUI items to the grid pane
        grid.add(addProductType, 57, 0, 10, 1);
        grid.add(labelOne, 35, 0, 20, 1);
        grid.add(labelTwo, 37, 1, 20, 1);
        grid.add(addList, 2, 3, 66, 40);
        grid.add(quantityField, 2, 45, 10, 1);
        grid.add(addField, 12, 45, 46, 1);
        grid.add(addButton, 60, 45, 20, 1);
        grid.add(cancelButton, 2, 46, 20, 1);
        grid.add(saveButton, 60, 46, 20, 1);

        this.addToGUI(grid);

        //Set the start focus to add field when the window opens
        addField.requestFocus();
    }
}
