/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.ui;

import foodNanny.app.EventNotification;
import foodNanny.db.EventDBO;
import foodNanny.db.Eventdb;
import foodNanny.db.JDBCfactory;
import static foodNanny.ui.Add.getTitle;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * Creates the History GUI window to be used for viewing Inventory editing
 * history. It contains two date pickers for selecting dates, as well as a list
 * view for displaying the contents of the history for a selected range.
 *
 * @author Jeff
 */
public class History extends GUI {

    //Declare the GUI variables
    private DatePicker startDatePicker;
    private DatePicker endDatePicker;
    private Button goBack;
    private LocalDate localDateStart;
    private LocalDate localDateEnd;
    private VBox vbox;
    private Label checkInlabel;
    private Label checkOutlabel;
    private ListView<GridPane> historyView;
    final Main app = Main.getInstance();
    private JDBCfactory factory;

    //Set the icon location for the Go Back Button
    private static final String GO_BACK
            = "http://icons.iconarchive.com/icons/custom-icon-design/flatastic-1/24/back-icon.png";

    /**
     * Returns "History" for the title
     *
     * @return The title
     */
    public static String getTitle() {
        return "History";
    }

    /**
     * Defines member variables and handles the Go Back Button
     */
    @Override
    protected void initialize() {
        Main.getStage().setTitle(getTitle());

        //Instantiate the variables
        factory = new JDBCfactory();
        historyView = new ListView();

        //Set up the image for the Go Back button
        Image goBackImage = new Image(GO_BACK);
        Node backImage = new ImageView(goBackImage);

        //Create and fill in the go back button
        goBack = new Button("Back", backImage);

        //Set the Go Back button bounds
        goBack.setMaxWidth(125);
        goBack.setMaxHeight(60);

        //Make the Go Back button take the user back to the main page
        goBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GUI goBack = new MainMenu();
                app.changePerspective(goBack);
            }
        });

        initUI();
    }

    /**
     * The main class of History that sets up the date pickers, as well as
     * handles their events.
     */
    private void initUI() {

        //Instantiate the variables
        vbox = new VBox(20);
        vbox.setStyle("-fx-padding: 10;");
        startDatePicker = new DatePicker();
        endDatePicker = new DatePicker();
        startDatePicker.setValue(LocalDate.now());
        localDateStart = LocalDate.now();
        localDateEnd = LocalDate.now();
        checkInlabel = new Label("History Start Date:");
        checkOutlabel = new Label("History End Date:");

        //Align the labels
        GridPane.setHalignment(checkInlabel, HPos.LEFT);
        GridPane.setHalignment(checkOutlabel, HPos.LEFT);

        //Set up the date pickers
        final Callback<DatePicker, DateCell> dayCellFactory
                = new Callback<DatePicker, DateCell>() {
                    @Override
                    public DateCell call(final DatePicker datePicker) {
                        return new DateCell() {
                            @Override
                            public void updateItem(LocalDate item, boolean empty) {

                                super.updateItem(item, empty);

                                //Disable selecting dates passed the original range
                                if (item.isBefore(
                                        startDatePicker.getValue().plusDays(1))) {
                                    setDisable(true);
                                    setStyle("-fx-background-color: #ffc0cb;");
                                }
                                long p = ChronoUnit.DAYS.between(
                                        startDatePicker.getValue(), item
                                );

                                //Set up the display for when moust hovering over
                                //a second date to see how far away the second
                                //date is from the original date
                                setTooltip(new Tooltip(
                                                "Checking history for " + p + " days")
                                );

                            }
                        };
                    }
                };

        //Set the second date picker automatically one ahead of the first
        endDatePicker.setDayCellFactory(dayCellFactory);
        endDatePicker.setValue(startDatePicker.getValue().plusDays(1));

        //Update the history based on the first date selected
        startDatePicker.setOnAction(event -> {
            localDateStart = startDatePicker.getValue();
            generateHistory();
        });

        //Update the history based on the second date selected
        endDatePicker.setOnAction(event -> {
            localDateEnd = endDatePicker.getValue();
            generateHistory();
        });

    }

    /**
     * This function creates the GUI window for the history using the Vbox and
     * Gridpane
     *
     */
    @Override
    protected void layout() {
        super.layout();
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        gridPane.add(goBack, 0, 1, 5, 1);
        gridPane.add(historyView, 0, 2, 41, 43);
        gridPane.add(checkInlabel, 42, 1, 7, 1);
        gridPane.add(startDatePicker, 42, 2, 7, 1);
        gridPane.add(checkOutlabel, 42, 9, 7, 1);
        gridPane.add(endDatePicker, 42, 10, 7, 1);

        vbox.getChildren().add(gridPane);

        this.addToGUI(gridPane);
    }

    /**
     * This function creates actual history events based off the date range
     * selected from the date pickers
     */
    public void generateHistory() {
        try {

            Eventdb eventdb = new Eventdb(factory);

            //Get a list of events based on the range.
            List<EventDBO> dates = eventdb.getRange(localDateStart, localDateEnd);
            ObservableList<GridPane> items = historyView.getItems();

            //Clear the list before displaying each time
            items.clear();

            //Loop through the list and add the individual events to the gridpane
            for (EventDBO eventDBO : dates) {
                EventNotification en = new EventNotification(eventDBO);

                items.add(en.getGrid());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
