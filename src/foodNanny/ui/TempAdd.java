/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package foodNanny.ui;

/**
 *
 * @author har09_000
 */
public class TempAdd {
    
    private int tempQTY;
    private int tempBarcodeNum;
    private String tempName;

    public void setTempName(String tempName) {
        this.tempName = tempName;
    }

    public String getTempName() {
        return tempName;
    }

    public int getTempQTY() {
        return tempQTY;
    }

    public int getTempBarcodeNum() {
        return tempBarcodeNum;
    }

    public void setTempQTY(int tempQTY) {
        this.tempQTY = tempQTY;
    }

    public void setTempBarcodeNum(int tempBarcodeNum) {
        this.tempBarcodeNum = tempBarcodeNum;
    }
    
    public TempAdd tempAdd(int quantity, int barcodeNumber) {
        TempAdd tempList = new TempAdd();
        System.out.println(quantity + " " + barcodeNumber);
        
        tempList.setTempQTY(quantity);
        tempList.setTempBarcodeNum(barcodeNumber);
        tempList.setTempName("Cheese");
        
        return tempList;
    }
    
}
