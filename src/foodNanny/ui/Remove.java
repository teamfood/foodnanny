/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.ui;

import foodNanny.app.Notification;
import foodNanny.db.BuyListdb;
import foodNanny.db.Eventdb;
import foodNanny.db.JDBCfactory;
import foodNanny.db.ProductDBO;
import foodNanny.db.StockListdb;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Handles the Remove window GUI for removing products from the Inventory
 * Stock List.
 * @author Jeff
 */
public class Remove extends GUI {

    //Declare gui elements
    private Button cancelButton;
    private Button saveButton;
    private Button removeButton;
    private Text labelOne;
    private Text labelTwo;
    private TextField removeField;
    private TextField quantityField;
    private ListView<GridPane> removeList;

    //Declare list storage
    private Map<String, Notification> tempRemove;
    private Map<String, ProductDBO> sendToBuyList;

    //Declare remaining variables
    private ProductDBO productDBO;
    private boolean cancel = false;

    //Set the icon image locations for the buttons
    private static final String TRASH
            = "http://icons.iconarchive.com/icons/wwalczyszyn/android-style/24/Trash-empty-icon.png";

    private static final String CANCEL
            = "http://icons.iconarchive.com/icons/hopstarter/sleek-xp-basic/24/Close-icon.png";

    private static final String OKAY
            = "http://icons.iconarchive.com/icons/oxygen-icons.org/oxygen/24/Actions-dialog-ok-apply-icon.png";

    /**
     * 
     * GET TITLE Return the GUI window title
     * 
     */
    public static String getTitle() {
        return "Remove from Inventory";
    }

    /**
     *
     * Initialize is the main class of Remove defining variables and
     * handling action events.
     * 
     */
    @Override
    protected void initialize() {
        Main.getStage().setTitle(getTitle());
        final Main app = Main.getInstance();

        //Instantiate the public member variables
        JDBCfactory factory = new JDBCfactory();
        removeList = new ListView();
        removeField = new TextField();
        quantityField = new TextField();
        tempRemove = new HashMap();
        sendToBuyList = new HashMap();
        labelOne = new Text("    Items to Remove");
        labelTwo = new Text("       Scan Items to remove from the list");

        //Set up the labels
        labelOne.setFont(new Font("Verdana", 35));
        labelTwo.setFont(Font.font("Verdana", FontWeight.BOLD, 13));

        //Set up the remove list
        removeList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        removeList.setStyle("-fx-border-color: black");

        //Set up the quantity field
        quantityField.setMaxWidth(40);
        quantityField.setPromptText("Qty");

        //Set the remove field default text
        removeField.setPromptText("Barcode Number");

        //Set up the image for the cancel button
        Image cancelButtonImage = new Image(CANCEL);
        Node cancelImage = new ImageView(cancelButtonImage);

        //Create and fill in the cancel button
        cancelButton = new Button(" Cancel", cancelImage);

        //Set cancel button bounds
        cancelButton.setMaxWidth(100);
        cancelButton.setMaxHeight(50);

        //Set up the image for the save button
        Image saveButtonImage = new Image(OKAY);
        Node saveImage = new ImageView(saveButtonImage);

        //Create and fill in the save button
        saveButton = new Button(" Save", saveImage);

        //Set save button bounds
        saveButton.setMaxWidth(100);
        saveButton.setMaxHeight(50);

        //Set up the image for the remove button
        Image stockListTrashImage = new Image(TRASH);
        Node stockListTrash = new ImageView(stockListTrashImage);

        //Create and fill in the remove button
        removeButton = new Button(" Remove", stockListTrash);

        //Style and set the bounds for the remove button
        removeButton.setStyle("-fx-font: 12 calibri");
        removeButton.setMaxWidth(100);
        removeButton.setMaxHeight(60);

        try {
            StockListdb stock = new StockListdb(factory);

            //Store the beginning stock list into a map
            Map<Integer, ProductDBO> currentStock = stock.getAll();

            //Iterate through the map getting the values out
            for (ProductDBO entry : currentStock.values()) {
                String barcode = entry.getBarcode();
                String name = entry.getName();
                int qty = entry.getQty();
                int tRemoved = entry.getQtyRemoved();

                //Create notifications with ProductDBO values
                Notification notification = new Notification(qty, name, barcode, tRemoved);

                //Fill up temporary remove list with notifications and get the grid
                tempRemove.put(barcode, notification);
                removeList.getItems().add(notification.getGrid());
            }

        } catch (SQLException ex) {
            Logger.getLogger(InventoryList.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Cancel Button event handler for canceling from remove
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GUI cancelMenu = new MainMenu();

                //Check the cancel boolean to see if anything has been changed
                //Then either cancel out or prompt for user decision
                if (!cancel) {
                    app.changePerspective(cancelMenu);
                } else {
                    cancelMenu.checkThenChange(cancelMenu, "You have not saved your current removals, "
                            + "are you sure you want to cancel");
                }
            }
        });

        //Remove Button event handler for removing products
        removeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                removeChange();
            }
        });

        //Remove Field event handler for pressing enter on the remove field
        removeField.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                removeChange();
            }
        });

        //Save Button event handler
        saveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JDBCfactory factory = new JDBCfactory();
                try {
                    StockListdb stockList = new StockListdb(factory);
                    BuyListdb buyList = new BuyListdb(factory);

                    String productNames = "";

                    //Clear out the stock list and create a new blank one
                    stockList.deleteTable();
                    stockList = new StockListdb(factory);

                    //Iterate through the temporary new stock list getting out the values
                    for (Map.Entry<String, Notification> entry : tempRemove.entrySet()) {
                        String barcodeKey = entry.getKey();
                        Notification value = entry.getValue();
                        int qty = value.getProductQuantity();

                        //Generate the new stock list
                        stockList.add(barcodeKey, qty);
                    }

                    //Iterate through the temporary buyList and get out the values
                    for (Map.Entry<String, ProductDBO> entry : sendToBuyList.entrySet()) {
                        String barcodeKey = entry.getKey();
                        ProductDBO value = entry.getValue();
                        int qtyRemoved = value.getQtyRemoved();

                        //Store the changes for the event
                        productNames += "   " + value.getQtyRemoved() + " of " + value.getName() + "\n";

                        //Set up the buy list
                        buyList.add(barcodeKey, qtyRemoved);

                    }

                    //Add the event for display later
                    Eventdb events = new Eventdb(factory);
                    events.addToday("Removed \n" + productNames, 2);

                    //Return to main menu
                    GUI save = new MainMenu();
                    app.changePerspective(save);
                } catch (SQLException ex) {
                    System.out.println("Unable to save.");
                }
            }
        });
    }

    /**
     *
     * This method handles setting up the GUI window by adding in the GUI
     * objects, as well as setting alignment and padding.
     *
     */
    @Override
    protected void layout() {
        super.layout();

        //Create the grid pane and set the padding, gaps, and alignment
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(5);
        grid.setVgap(5);
        grid.setPadding(new Insets(10, 10, 10, 10));

        //Add the GUI items to the grid pane
        grid.add(labelOne, 13, 0, 10, 9);
        grid.add(labelTwo, 20, 7, 9, 5);
        grid.add(removeList, 2, 14, 29, 10);
        grid.add(quantityField, 2, 25, 3, 5);
        grid.add(removeField, 6, 25, 20, 5);
        grid.add(removeButton, 26, 25, 20, 5);
        grid.add(cancelButton, 2, 33, 20, 5);
        grid.add(saveButton, 26, 33, 20, 5);

        this.addToGUI(grid);

        //Set the start focus to remove field when the window opens
        removeField.requestFocus();
    }

    /**
     * 
     * This method is called from the remove field and remove
     * change event handlers. It takes the information from the quantity field
     * and remove fields to generate the buy list, as well as update the
     * information for the new stock list.
     *
     */
    public void removeChange() {
        try {
            int qty;
            String barcode;

            //Default quantity to 1
            if (quantityField.getText().equals("")) {
                qty = 1;
            } else {
                qty = Integer.parseInt(quantityField.getText());
                if (qty == 0) {
                    qty = 1;
                }
            }

            //Get the barcode from the remove field
            barcode = removeField.getText();

            //Check if the product type exists
            if (tempRemove.containsKey(barcode)) {

                //Create a new notification
                Notification n = tempRemove.get(barcode);
                productDBO = new ProductDBO();

                //Check that there are still products to remove from
                if (n.getProductQuantity() > 0) {

                    //Can't remove products past 0
                    if (n.getProductQuantity() - qty < 0) {
                        Error error = new Error();
                        error.createView("Cannot remove more than total quantity");
                    } else {

                        //Keep track of how many products were removed
                        productDBO.setQtyRemoved(n.getProductRemoved() + qty);
                        productDBO.setName(n.getProductName());

                        //Set thew new product quantity and the product removed values
                        n.setProductQuantity(n.getProductQuantity() - qty);
                        n.setProductRemoved(n.getProductRemoved() + qty);

                        //Send the information to the buy list
                        sendToBuyList.put(barcode, productDBO);

                        //Set cancel to true for the prompt when cancel button is pressed.
                        cancel = true;
                    }

                }

                //If there's no more of a product, then remove it from the list.
                if (n.getProductQuantity() == 0) {
                    removeList.getItems().remove(n.getGrid());
                }
            } else {
                Error error = new Error();
                error.createView("Product type " + removeField.getText() + " does not exist");
            }

            //Clear the field upon removing, and reset the focus to the remove field.
            removeField.clear();
            removeField.requestFocus();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
