/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.ui;

import foodNanny.app.EventNotification;
import foodNanny.db.EventDBO;
import foodNanny.db.Eventdb;
import foodNanny.db.JDBCfactory;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Creates the Main Menu GUI window to be used as the home menu for displaying
 * recent events, as well as providing a base hub to access the other GUI
 * windows.
 *
 * @author Jeff
 */
public class MainMenu extends GUI {

    //Declare the variables
    private static MainMenu mainView = new MainMenu();
    private float totalMoney;

    public boolean isConnected;
    public boolean checkScanner = false;

    private Button removeInventory;
    private Button addInventory;
    private Button viewInventory;
    private Button viewHistory;
    private Button addWholeseller;

    private Label moneySavedLabel;
    private Label moneySavedAmount;
    private Label todaysEventsLabel;

    private static ListView<GridPane> eventList;

    /**
     * The main method of the MainMenu class. It defines member variables, as
     * well as handles all the events for the GUI.
     */
    @Override
    protected void initialize() {
        Main.getStage().setTitle(getTitle());
        final Main app = Main.getInstance();

        //Instantiate the variables
        moneySavedLabel = new Label();
        moneySavedAmount = new Label();
        moneySavedLabel.setText("Saved:");
        moneySavedAmount.setText("$" + totalMoney);
        todaysEventsLabel = new Label("Today's Events");
        eventList = new ListView();
        addWholeseller = new Button("Add Wholeseller");
        removeInventory = new Button("Remove Inventory");
        addInventory = new Button("Add Inventory");
        viewInventory = new Button("View Inventory");
        viewHistory = new Button("View History");

        //Style the GUI
        moneySavedAmount.setTextFill(Color.LAWNGREEN);
        moneySavedLabel.setFont(new Font("Calibri", 20));
        moneySavedAmount.setFont(new Font("Calibri", 20));
        todaysEventsLabel.setFont(new Font("Calibri", 35));
        eventList.setStyle("-fx-border-color: black;");
        removeInventory.setStyle("-fx-font: 26 calibri");
        addInventory.setStyle("-fx-font: 26 calibri");
        viewInventory.setStyle("-fx-font: 26 calibri");
        viewHistory.setStyle("-fx-font: 16 calibri");

        //Set up the button bounds
        removeInventory.setMaxWidth(240);
        removeInventory.setMaxHeight(60);
        addInventory.setMaxWidth(240);
        addInventory.setMaxHeight(60);
        viewInventory.setMaxWidth(240);
        viewInventory.setMaxHeight(60);
        viewHistory.setMaxWidth(120);
        viewHistory.setMaxHeight(10);

        eventList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        JDBCfactory factory = new JDBCfactory();

        //Create and display the recent events
        try {
            Eventdb eventdb = new Eventdb(factory);

            List<EventDBO> today = eventdb.getToday();

            //Store the events into the items list
            ObservableList<GridPane> items = eventList.getItems();

            //Add the events to the gridpane for displaying
            for (EventDBO eventDBO : today) {
                EventNotification en = new EventNotification(eventDBO);
                items.add(en.getGrid());
            }

        } catch (SQLException ex) {
            System.err.println(ex);
        }

        //Pops up a window for adding a new wholesaler
        addWholeseller.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                NewWholeseller wholeseller = new NewWholeseller();
                wholeseller.createWholeseller();
            }
        });

        //Takes the user to the Remove Inventory page
        removeInventory.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GUI rInv = new Remove();
                app.changePerspective(rInv);

            }
        });

        //Takes the user to the Add Inventory Page
        addInventory.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GUI aInv = new Add();
                app.changePerspective(aInv);

            }
        });

        //Takes the user to the View Inventory Page
        viewInventory.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GUI iList = new InventoryList();
                app.changePerspective(iList);
            }
        });

        //Takes the user to the View History Page
        viewHistory.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GUI history = new History();
                app.changePerspective(history);
            }
        });
    }

    /**
     * Sets up the layout of the GUI by adding in GUI items to the gridpane, as
     * well as handling gap and padding.
     */
    @Override
    protected void layout() {
        super.layout();

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(5);
        grid.setVgap(5);
        grid.setPadding(new Insets(10, 10, 10, 10));

        grid.add(addWholeseller, 3, 80, 50, 30);
        // grid.add(moneySavedLabel, 90, 0, 110, 1);
        grid.add(moneySavedAmount, 105, 0, 120, 1);
        grid.add(todaysEventsLabel, 64, 8, 60, 1);
        grid.add(eventList, 55, 9, 60, 80);
        grid.add(removeInventory, 3, 10, 80, 30);
        grid.add(addInventory, 3, 24, 80, 30);
        grid.add(viewInventory, 3, 38, 80, 30);
        grid.add(viewHistory, 90, 80, 50, 30);

        this.addToGUI(grid);
    }

    /**
     * Gets the title of this perspective (used to display title at the top of
     * the page and to display the name of the perspective in the "change
     * perspective" menu)
     *
     * @return The title of this perspective
     */
    public static String getTitle() {
        return "Main Menu";
    }
}
