/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package foodNanny.ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Used as a template for creating pop up error windows.
 * @author Jeff
 */
public class Error {
    
    /**
     * Creates the actual window view taking in a phrase String as a parameter
     * to be used as the actual error message.
     * @param phrase 
     */
    public void createView(String phrase) {
        final Stage error = new Stage();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        Label promptLabel = new Label(phrase);
        grid.add(promptLabel, 0, 0, 4, 1);
       
        Button OK = new Button("OK");
        OK.setOnAction(new EventHandler<ActionEvent> () {
            @Override
            public void handle(ActionEvent event) {
                error.close();
            }
        });
        grid.add(OK, 5, 0);
        
        Scene scene = new Scene(grid);
        error.setScene(scene);
        error.show();
    }
}
