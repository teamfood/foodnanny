/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.ui;

import foodNanny.db.AlreadyExistsException;
import foodNanny.db.JDBCfactory;
import foodNanny.db.ProductTypesdb;
import foodNanny.db.WholesellerDBO;
import foodNanny.db.Wholesellerdb;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Acts as the GUI for creating new product types by getting the necessary
 * product information from the user.
 *
 * @author Jeff
 */
public class ProductTypeManager {

    private GridPane grid;
    private Label title;
    private Label wholeSalerName;
    private Label name;
    private TextField getName;
    private Label barcode;
    private TextField getBarcode;
    private int daysGood;
    private Label daysGoodLabel;
    private TextField getDaysGood;
    private double price;
    private Label priceLabel;
    private TextField getPrice;
    private Button OK;
    private Stage newProductType = new Stage();
    private ComboBox comboBox;

    public void createView() {

        //Instantiate the variables
        grid = new GridPane();
        title = new Label("Enter the following data");
        name = new Label(" Name:        ");
        getName = new TextField();
        barcode = new Label(" Barcode:      ");
        getBarcode = new TextField();
        wholeSalerName = new Label(" Wholesaler: ");
        daysGoodLabel = new Label(" Days Good: ");
        getDaysGood = new TextField();
        priceLabel = new Label(" Price: ");
        getPrice = new TextField();
        OK = new Button("OK");
        ObservableList<WholesellerDBO> options = FXCollections.observableArrayList();
        comboBox = new ComboBox(options);
        JDBCfactory fac = new JDBCfactory();

        //Set the padding and gaps
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setHgap(5);
        grid.setVgap(5);

        //Style and set the bounds for the window
        title.setFont(new Font("Verdana", 20));
        OK.setMaxHeight(30);
        OK.setMaxWidth(75);
        getName.setMaxWidth(160);
        getBarcode.setMaxWidth(160);
        getDaysGood.setMaxWidth(160);
        getPrice.setMaxWidth(160);
        comboBox.setMaxWidth(160);

        try {
            Wholesellerdb wholesellerdb = new Wholesellerdb(fac);
            List<WholesellerDBO> wholesellers;

            //Get the wholesaler name and add it to the comboBox
            wholesellers = wholesellerdb.getAllShallow();
            options.addAll(wholesellers);

        } catch (SQLException ex) {
            System.err.println(ex);
        }

        //Create the new product
        OK.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String newName = getName.getText();
                
                //Verify a name was entered
                if (newName.equals("")) {
                    Error newNameError = new Error();
                    newNameError.createView("Please enter a product name");
                    return;
                }

                String newBarcode = getBarcode.getText();
                
                //Verify a barcode was entered
                if (newBarcode.equals("")) {
                    Error barcodeError = new Error();
                    barcodeError.createView("Please enter a valid barcode");
                    return;
                }

                //Verify a number was entered and convert string to int
                try {
                    daysGood = Integer.parseInt(getDaysGood.getText());
                } catch (NumberFormatException nex) {
                    Error daysGoodError = new Error();
                    daysGoodError.createView("Please enter number of days good");
                    return;
                }

                //Verify a double was entered and convert string to double
                try {
                    price = Double.parseDouble(getPrice.getText());
                } catch (NumberFormatException dex) {
                    Error priceError = new Error();
                    priceError.createView("Please enter a dollar amount for the price");
                    return;
                }

                WholesellerDBO newWholeseller = (WholesellerDBO) comboBox.getValue();
                
                //Verify a wholesaler was selected
                if (newWholeseller == null) {
                    Error wholeError = new Error();
                    wholeError.createView("Please select a wholesaler");
                    return;
                }
                
                //Create the new product type
                createNewProduct(newName, newBarcode, newWholeseller, daysGood, price);
            }
        });

        //Fill the grid
        grid.add(wholeSalerName, 0, 10);
        grid.add(comboBox, 1, 10);
        grid.add(title, 0, 0, 4, 1);
        grid.add(name, 0, 2);
        grid.add(getName, 1, 2);
        grid.add(barcode, 0, 4);
        grid.add(getBarcode, 1, 4);
        grid.add(daysGoodLabel, 0, 6);
        grid.add(getDaysGood, 1, 6);
        grid.add(priceLabel, 0, 8);
        grid.add(getPrice, 1, 8);
        grid.add(OK, 1, 12);

        Scene scene = new Scene(grid, 265, 400);
        newProductType.setScene(scene);
        newProductType.show();
    }

    /**
     * Creates a new product type based off the values input from the user
     * in the GUI window.
     * @param name Takes in a new product name as a String.
     * @param barcode Takes in a new product bar code as a String.
     * @param wholeseller Takes in a wholesaler as a Wholeseller.
     * @param daysGood Takes in a days good as an Integer.
     * @param price Takes in a new product price as a Double.
     */
    public void createNewProduct(String name, String barcode, WholesellerDBO wholeseller,
            int daysGood, double price) {
        JDBCfactory factory = new JDBCfactory();
        try {
            int id = wholeseller.getId();
            ProductTypesdb productType = new ProductTypesdb(factory);
            
            //Fill in the new product type
            productType.add(name, daysGood, barcode, id, 3, price);
            newProductType.close();

        } catch (SQLException ex) {
            System.out.println(ex);
        } catch (AlreadyExistsException ex) {
            Error productAddError = new Error();
            productAddError.createView("Product already exists.");
        }
    }
}
