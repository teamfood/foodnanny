/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.ui;

import foodNanny.db.AlreadyExistsException;
import foodNanny.db.JDBCfactory;
import foodNanny.db.WholesellerDBO;
import foodNanny.db.Wholesellerdb;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Handles editing wholesaler information.
 *
 * @author Jeff
 */
public class NewWholeseller {

    //Declare the variables
    private Label name;
    private Label email;
    private TextField nameField;
    private TextField emailField;
    private Button OK;

    /**
     * The main function of NewWholeSeller. It instantiates the member variables
     * and handles the events related to editing a wholesaler's info.
     */
    public void createWholeseller() {
        //Instantiate the variables
        final Stage stage = new Stage();
        GridPane grid = new GridPane();
        name = new Label("Name:");
        email = new Label("Email:");
        nameField = new TextField();
        emailField = new TextField();
        OK = new Button("OK");

        //Setup window
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        OK.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JDBCfactory factory = new JDBCfactory();
                try {
                    //Make sure there's something in the email field
                    if (emailField.getText().equals("") || nameField.getText().equals("")) {
                        Error error = new Error();
                        error.createView("One or more fields are empty");
                    } else {
                        //Set a default email text
                        Wholesellerdb whole = new Wholesellerdb(factory);
                        String emailText = "I would like to order the following products.\nThanks.";
                        WholesellerDBO wholeDBO = new WholesellerDBO(nameField.getText(), emailField.getText(), emailText);
                        whole.add(wholeDBO);
                        stage.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(NewWholeseller.class.getName()).log(Level.SEVERE, null, ex);
                } catch (AlreadyExistsException ex) {
                    Error wholeAddError = new Error();
                    wholeAddError.createView("Wholesaler already exists.");;
                }
            }
        });

        //Add the GUI elements to the Gridpane
        grid.add(name, 0, 2);
        grid.add(nameField, 1, 2);
        grid.add(email, 0, 4);
        grid.add(emailField, 1, 4);
        grid.add(OK, 1, 6);

        Scene scene = new Scene(grid, 265, 400);
        stage.setScene(scene);
        stage.show();
    }
}
