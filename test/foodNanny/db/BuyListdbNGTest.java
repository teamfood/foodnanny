/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author justgage
 */
public class BuyListdbNGTest {


    public BuyListdbNGTest() {
    }

    /**
     * Test of deleteTable method, of class BuyListdb.
     *
     */
    @Test(priority = 0)
    public void testDeleteTable() {
        JDBCfactory factory = new JDBCfactory(true);
        BuyListdb buylist;
        try {
            buylist = new BuyListdb(factory);
            buylist.deleteTable();
        } catch (SQLException ex) {
            System.err.println("Table couldn't be deleted ");
            System.err.println(ex);
        }

    }


    /**
     * Test of add method, of class BuyListdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 2)
    public void testAdd_int_long() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        BuyListdb buylist = new BuyListdb(factory);

        buylist.add(0, new Date().getTime());
        buylist.add(0, new Date().getTime());
        buylist.add(0, new Date().getTime());
        buylist.add(1, new Date().getTime());
        buylist.add(4, new Date().getTime());
    }

    /**
     * Test of add method, of class BuyListdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 2)
    public void testAdd_int() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        BuyListdb buylist = new BuyListdb(factory);

        buylist.add(2);
        buylist.add(2);
        buylist.add(2);
        buylist.add(1);
    }

    /**
     * Test of remove method, of class BuyListdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 4)
    public void testRemove() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        BuyListdb buylist = new BuyListdb(factory);

        assertEquals(buylist.remove(2, 3), 3);
        assertEquals(buylist.remove(1, 1), 1);
        assertEquals(buylist.remove(4, 100), 1);

    }

    @Test(priority = 5)
    public void testGetAllRaw() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        BuyListdb buylist = new BuyListdb(factory);

        List<Map<String, Object>> all = buylist.getAllRaw();

        assertFalse(all.isEmpty());

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        for (Map<String, Object> item : all) {
            System.err.println(item.get("ID") + " -> "
                    + item.get("productID") + " "
                    + dateFormat.format(item.get("dateAdded")));
        }

    }

    /**
     * Test of get method, of class BuyListdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 5)
    public void testGet() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        BuyListdb buylist = new BuyListdb(factory);

        Map<String, Object> first = buylist.get(1);

        assertEquals(first.get("productID"), 2);

    }

    /**
     * Test of getAll method, of class BuyListdb.
     * @throws java.lang.Exception
     */
    @Test(priority = 5)
    public void testGetAll() throws Exception {

        JDBCfactory factory = new JDBCfactory(true);
        BuyListdb buylist = new BuyListdb(factory);

        Map<Integer, ProductDBO> all = buylist.getAll();

        assertFalse(all.isEmpty());

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        System.err.println("--Getall--");
        for (Integer itemID : all.keySet()) {
            ProductDBO item = all.get(itemID);
            System.err.println(itemID + " -> "
                    + item.getProductTypeID()+ " "
                    + item.getQty() + " "
                    + dateFormat.format(item.getDateAdded()));
        }
        System.err.println("");
    }

}
