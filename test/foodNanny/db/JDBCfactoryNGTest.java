/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Gage
 */
public class JDBCfactoryNGTest {

    public JDBCfactoryNGTest() {
    }

    /**
     * Test of createConnection method, of class JDBCfactory.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateConnection() {
        JDBCfactory fac = new JDBCfactory("testingDB");

        Connection con = null;
        try {
            con = fac.createConnection();

            Statement statement = con.createStatement();

            statement.executeUpdate("drop table if exists person");
            statement.executeUpdate("create table person (id integer, name string)");
            statement.executeUpdate("insert into person values(1, 'leo')");
            statement.executeUpdate("insert into person values(2, 'yui')");
        } catch (SQLException ex) {
            Assert.fail(ex.getMessage());               
            
        } finally {

            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Assert.fail();
                }
            }
        }

    }

}
