/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.SQLException;
import java.util.Map;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Gage
 */
public class ProductTypesdbNGTest {

    public ProductTypesdbNGTest() {

    }

    /**
     * Test of deleteTable method, of class ProductTypesdb.
     * @throws java.lang.Exception
     */
    @Test(priority = 1)
    public void testDeleteTable() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        ProductTypesdb productType = new ProductTypesdb(factory);
        productType.deleteTable();
    }

    /**
     * Test of add method, of class ProductTypesdb.
     *
     * @throws java.sql.SQLException
     */
    @Test(priority = 2)
    public void testAdd() throws SQLException, AlreadyExistsException {
        JDBCfactory factory = new JDBCfactory(true);
        ProductTypesdb productType = new ProductTypesdb(factory);

        productType.add("Cheese", 3, "1000", 1, 100, 10.50);
        productType.add("Hot Dogs", 3, "12000", 1, 5, 2.00);
    }

    /**
     * Test of getByBarcode method, of class ProductTypesdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 3)
    public void testGetByBarcode() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        ProductTypesdb productType = new ProductTypesdb(factory);

        ProductDBO product = productType.getByBarcode("1000");
        assertNull(productType.getByBarcode("123123123"));

        assertNotNull(product);

        assertEquals(product.getName(), "Cheese");
    }

    /**
     * Test of getByID method, of class ProductTypesdb.
     */
    @Test(priority = 3)
    public void testGetByID() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        ProductTypesdb productType = new ProductTypesdb(factory);

        ProductDBO cheese = productType.getByID(1);
        ProductDBO notCheese = productType.getByID(2);
        ProductDBO notAnything = productType.getByID(-1);

        assertNotNull(cheese);
        assertNull(notAnything);

        assertEquals(cheese.getName(), "Cheese");
        assertNotEquals(notCheese.getName(), "Cheese");
    }

    /**
     * Test of getAll method, of class ProductTypesdb.
     */
    @Test(priority = 3)
    public void testGetAll() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        ProductTypesdb productType = new ProductTypesdb(factory);

        Map<Integer, ProductDBO> all = productType.getAll();

        for (ProductDBO productDBO : all.values()) {
            System.err.println(productDBO);
        }
    }

    /**
     * Test of getAllbyWholseller method, of class ProductTypesdb.
     */
    @Test
    public void testGetAllbyWholseller() throws Exception {
    }

    /**
     * Test of exists method, of class ProductTypesdb.
     */
    @Test(priority = 3)
    public void testExists() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        ProductTypesdb productType = new ProductTypesdb(factory);

        assertTrue(productType.exists("Cheese", ""));
        assertTrue(productType.exists("", "1000"));
        assertTrue(productType.exists("Cheese", "1000"));
        assertFalse(productType.exists("asdf", "asdf"));
    }

}
