/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package foodNanny.db;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Gage
 */
public class ListdbNGTest {
       /**
     * Test of deleteTable method, of class Listdb.
     *
     */
    @Test(priority = 0)
    public void testDeleteTable() {
        JDBCfactory factory = new JDBCfactory(true);
        Listdb list;
        try {
            list = new Listdb(factory);
            list.deleteTable();
        } catch (SQLException ex) {
            System.err.println("Table couldn't be deleted ");
            System.err.println(ex);
        }

    }

    /**
     * Test of add method, of class Listdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 2)
    public void testAdd_int_long() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        Listdb list = new Listdb(factory);

        list.add(0, new Date().getTime());
        list.add(0, new Date().getTime());
        list.add(0, new Date().getTime());
        list.add(1, new Date().getTime());
        list.add(4, new Date().getTime());
    }

    /**
     * Test of add method, of class Listdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 2)
    public void testAdd_int() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        Listdb list = new Listdb(factory);

        list.add(2);
        list.add(2);
        list.add(2);
        list.add(1);
    }

    /**
     * Test of remove method, of class Listdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 4)
    public void testRemove() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        Listdb list = new Listdb(factory);

        assertEquals(list.remove(2, 3), 3);
        assertEquals(list.remove(1, 1), 1);
        assertEquals(list.remove(4, 100), 1);

    }

    @Test(priority = 5)
    public void testGetAllRaw() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        Listdb list = new Listdb(factory);

        List<Map<String, Object>> all = list.getAllRaw();

        assertFalse(all.isEmpty());

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        for (Map<String, Object> item : all) {
            System.err.println(item.get("ID") + " -> "
                    + item.get("productID") + " "
                    + dateFormat.format(item.get("dateAdded")));
        }

    }

    /**
     * Test of get method, of class Listdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 5)
    public void testGet() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        Listdb list = new Listdb(factory);

        Map<String, Object> first = list.get(1);

        assertEquals(first.get("productID"), 2);

    }

    /**
     * Test of getAll method, of class Listdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 5)
    public void testGetAll() throws Exception {

        JDBCfactory factory = new JDBCfactory(true);
        Listdb list = new Listdb(factory);

        Map<Integer, ProductDBO> all = list.getAll();

        assertFalse(all.isEmpty());

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        System.err.println("--Getall--");
        for (Integer itemID : all.keySet()) {
            ProductDBO item = all.get(itemID);
            System.err.println(itemID + " : { "
                    + "\t ProductID" + item.getProductTypeID() + "\n "
                    + "\t QTY: " + item.getQty() + " \n"
                    + "\t DateAdded:" + dateFormat.format(item.getDateAdded()) 
                    + "\n}\n\n"
            );
        }
        System.err.println("");
    }
    
}
