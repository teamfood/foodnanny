/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.SQLException;
import java.util.List;
import org.testng.Assert;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Gage
 */
public class EmailsdbNGTest {

    public EmailsdbNGTest() {
    }

    /**
     * Test of add method, of class Emailsdb.
     */
    @Test(priority = 1)
    public void testAdd() throws SQLException {
        JDBCfactory jdbc = new JDBCfactory(true);
        Emailsdb emaildb = new Emailsdb(jdbc);

        emaildb.add(new EmailDBO("admin", "password"));
        emaildb.add(new EmailDBO("coolness", "can'tguessthis"));
    }

    /**
     * Test of getAll method, of class Emailsdb.
     */
    @Test(priority = 2)
    public void testGetAll() throws SQLException {
        JDBCfactory jdbc = new JDBCfactory(true);
        Emailsdb emaildb = new Emailsdb(jdbc);

        List<EmailDBO> all = emaildb.getAll();

        EmailDBO first = all.get(0);
        assertEquals(first.getUsername(), "admin");
        assertEquals(first.getPassword(), "password");

    }

    /**
     * Test of get method, of class Emailsdb.
     */
    @Test(priority = 2)
    public void testGet() throws Exception {
        JDBCfactory jdbc = new JDBCfactory(true);
        Emailsdb emaildb = new Emailsdb(jdbc);

        EmailDBO first = emaildb.get(1);
        assertEquals(first.getUsername(), "admin");
        assertEquals(first.getPassword(), "password");

    }

}
