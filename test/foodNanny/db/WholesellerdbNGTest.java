/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.SQLException;
import java.util.List;
import static org.testng.Assert.*;
import org.testng.FileAssert;
import org.testng.annotations.Test;

/**
 *
 * @author Gage
 */
public class WholesellerdbNGTest {

    public WholesellerdbNGTest() {
    }

    /**
     * Test of deleteTable method, of class Wholesellerdb.
     */
    @Test(priority = 0)
    public void testDeleteTable() throws Exception {
        JDBCfactory fac = new JDBCfactory(true);
        Wholesellerdb wholesellerdb = new Wholesellerdb(fac);

        wholesellerdb.deleteTable();
    }

    /**
     * Test of add method, of class Wholesellerdb.
     */
    @Test(priority = 1)
    public void testAdd() throws SQLException, AlreadyExistsException {
        JDBCfactory fac = new JDBCfactory(true);
        Wholesellerdb wholesellerdb = new Wholesellerdb(fac);

        wholesellerdb.add(new WholesellerDBO("Walmart", "walmart@gmail.com", "thanks for your buisness!"));
        wholesellerdb.add(new WholesellerDBO("Costco", "thisIsCheepStuff@gmail.com", "You cheated me!!!"));
    }

    /**
     * Test of getAllShallow method, of class Wholesellerdb.
     */
    @Test(priority = 2)
    public void testGetAllShallow() throws Exception {
        JDBCfactory fac = new JDBCfactory(true);
        Wholesellerdb wholesellerdb = new Wholesellerdb(fac);

        List<WholesellerDBO> all = wholesellerdb.getAllShallow();

        assertNotNull(all);
        assertEquals(all.size(), 2);

        System.err.println("AllShallow");

        for (WholesellerDBO wholeDBO : all) {
            System.err.println(wholeDBO);
        }
    }

    /**
     * Test of exists method, of class Wholesellerdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 2)
    public void testExists() throws Exception {
        JDBCfactory fac = new JDBCfactory(true);
        Wholesellerdb wholesellerdb = new Wholesellerdb(fac);

        assertTrue(wholesellerdb.exists("Walmart"));
        assertTrue(wholesellerdb.exists("Costco"));
        assertFalse(wholesellerdb.exists("lkajs;lfjka;sdf'`"));
        assertFalse(wholesellerdb.exists(""));
    }

    /**
     * Test of update method, of class Wholesellerdb.
     */
    @Test(priority = 3)
    public void testUpdate() throws SQLException {
        JDBCfactory fac = new JDBCfactory(true);
        Wholesellerdb wholesellerdb = new Wholesellerdb(fac);
        
        WholesellerDBO whole = new WholesellerDBO("Walmart2", "email2@gmail.com", "This is a changed message");
        whole.setId(1);
        
        wholesellerdb.update(whole);
        
    }

}
