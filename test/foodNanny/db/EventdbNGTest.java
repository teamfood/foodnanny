/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Date;
import java.util.List;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Gage
 */
public class EventdbNGTest {

    public EventdbNGTest() {
    }

    /**
     * Test of deleteTable method, of class Eventdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 0)
    public void testDeleteTable() throws Exception {
        JDBCfactory fac = new JDBCfactory(true);
        Eventdb eventdb = new Eventdb(fac);

        eventdb.deleteTable();
    }

    /**
     * Test of add method, of class Eventdb.
     *
     * @throws java.sql.SQLException
     */
    @Test(priority = 1)
    public void testAdd() throws SQLException {
        JDBCfactory fac = new JDBCfactory(true);
        Eventdb eventdb = new Eventdb(fac);

        eventdb.add("This happened on my birthday", 1, LocalDateTime.of(1, 10, 1, 0, 0));
        eventdb.add("This happened after my birthday", 2, LocalDateTime.of(1, 12, 1, 0, 0));
        eventdb.add("Different Event", 2, LocalDateTime.of(10, 12, 1, 0, 0));

    }

    /**
     * Test of addToday method, of class Eventdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 1)
    public void testAddToday() throws Exception {
        JDBCfactory fac = new JDBCfactory(true);
        Eventdb eventdb = new Eventdb(fac);
        eventdb.addToday("This happened today", 2);
        eventdb.addToday("This happened today as well", 1);

    }

    /**
     * Test of getRange method, of class Eventdb.
     *
     * @throws java.lang.Exception
     */
    @Test(priority = 2)
    public void testGetRange() throws Exception {
        JDBCfactory fac = new JDBCfactory(true);
        Eventdb eventdb = new Eventdb(fac);

        LocalDate now = LocalDate.now();
        LocalDate birthday = LocalDate.of(0, 10, 1);
        LocalDate other = LocalDate.of(1909, 11, 28);
        List<EventDBO> range = eventdb.getRange(now, now);
        List<EventDBO> rangeAll = eventdb.getRange(birthday, now);
        List<EventDBO> rangePart = eventdb.getRange(other, now);

        assertEquals(range.size(), 2);
        assertEquals(rangeAll.size(), 5);
        assertEquals(rangePart.size(), 2);
        EventDBO event = range.get(0);
        assertEquals(event.getMessage(), "This happened today");

        System.err.println(event.getDate().getHour()%12 + ":" + event.getDate().getMinute());

    }

}
