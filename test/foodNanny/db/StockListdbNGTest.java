/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodNanny.db;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Gage
 */
public class StockListdbNGTest {

    public StockListdbNGTest() {
    }

    /**
     * Test of deleteTable method, of class StockListdb.
     *
     */
    @Test(priority = 0)
    public void testDeleteTable() {
        JDBCfactory factory = new JDBCfactory(true);
        StockListdb stocklist;
        try {
            stocklist = new StockListdb(factory);
            stocklist.deleteTable();
        } catch (SQLException ex) {
            System.err.println("Table couldn't be deleted ");
            System.err.println(ex);
        }

    }


    /**
     * Add by barcode
     */
    @Test(priority = 2)
    public void testAdd_String_int() throws Exception {
        JDBCfactory factory = new JDBCfactory(true);
        StockListdb stocklist = new StockListdb(factory);

        stocklist.add("1000", 10);

    }

}
